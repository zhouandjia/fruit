//
//  GameCenterHelper.h
//  FruitGames
//
//  Created by Static Ga on 12-12-16.
//
//

#import <Foundation/Foundation.h>

@interface GameCenterHelper : NSObject
{
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
}

@property (assign, readonly) BOOL gameCenterAvailable;

+ (GameCenterHelper *)sharedInstance;
- (void)authenticateLocalUser;

@end
