//
//  GameMenuScene.h
//  FruitGames
//
//  Created by 周凯俊 on 12-11-18.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SinaWeibo.h"
@interface GameMenuScene : CCLayer <SinaWeiboDelegate,SinaWeiboRequestDelegate,UITextViewDelegate>{
    BOOL didShowSubItem;
    BOOL didShowGameCenterSubItem;
    BOOL playBackMusic;
    
    BOOL didShowSina;
}
@property (nonatomic, retain) UITextView *textView;
@property (nonatomic, retain) UIView *roteView;
+ (CCScene *)scene;

@end
