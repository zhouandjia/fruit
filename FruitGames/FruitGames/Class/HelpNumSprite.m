//
//  HelpNumSprite.m
//  FruitGames
//
//  Created by 周凯俊 on 12-12-8.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "HelpNumSprite.h"
#import "CCFruitsData.h"

#define Help_Num @"fangdajingjiaobiao.png"

@implementation HelpNumSprite

+ (HelpNumSprite*)sprite
{
    return [HelpNumSprite spriteWithNum:[CCFruitsData getDJNum]];
}

+ (HelpNumSprite*)spriteWithNum:(NSInteger)_num
{
    HelpNumSprite *sprite = [HelpNumSprite spriteWithFile:Help_Num];
    [sprite addNum:_num];
    return sprite;
}

- (void)addNum:(NSInteger)_num
{
    CGSize cttSize = [self contentSize];
    NSString *numStr = [NSString stringWithFormat:@"%d",_num];
    NSInteger num = _num > 0 ? 0 : 1;
    while(_num)
    {
        _num /= 10;
        num++;
    }
    CCLabelTTF *numLabel = [CCLabelTTF labelWithString:numStr fontName:@"Thonburi" fontSize:cttSize.width/num];
    [numLabel setPosition:CGPointMake(cttSize.width/2, cttSize.height/2)];
    [self addChild:numLabel];
}

@end
