//
//  GamePlayScene.h
//  FruitGames
//
//  Created by 周凯俊 on 12-11-25.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class FruitSprite;

@interface GamePlayScene : CCLayer {
    NSMutableArray *showArray;
    NSMutableArray *putArray;
    
    FruitSprite *moveSprite;
    CGPoint beginPoint;
}

+ (CCScene *)scene;

@end
