//
//  GameCompareScene.h
//  FruitGames
//
//  Created by 周凯俊 on 12-11-29.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameCompareScene : CCLayer {
    
}

+ (CCScene*)scene:(NSArray *)_array;
- (id)initWithArray:(NSArray *)_array;

@end
