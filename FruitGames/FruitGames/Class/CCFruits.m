//
//  CCFruits.m
//  FruitGames
//
//  Created by 周凯俊 on 12-11-24.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "CCFruits.h"
#include "FruitSprite.h"

#define Fruit_Name @"Fruit"
#define Fruit_Num 15
#define First_Fruits_Num 1
#define Chance_Num 3

static CCFruits *fruits = NULL;

@implementation CCFruits

+ (CCFruits *)shared
{
    if(!fruits)
    {
        fruits = [[CCFruits alloc] init];
    }
    return fruits;
}

- (id)init
{
    if(self = [super init])
    {
        fruitsAry = [[NSMutableArray alloc] init];
        index = First_Fruits_Num;
        chanceNum = Chance_Num;
    }
    
    return self;
}

- (void)clearFruits
{
    [fruits release];
    fruits = NULL;
}

- (void)dealloc
{
    [fruitsAry release];
    fruitsAry = NULL;
    [super dealloc];
}

- (NSMutableArray *)getFruits
{
    return fruitsAry;
}

- (void)indexAdd
{
    index++;
}

- (NSInteger)getIndex
{
    return index;
}

- (void)chanceNumSub
{
    if (chanceNum <= 0) {
        chanceNum = 0;
        return;
    }
    chanceNum--;
}

- (NSInteger)getChanceNum
{
    return chanceNum;
}

- (void)fruitsRange
{
    [self fruitsRange:index];
}

- (NSMutableArray *)getFruitsArray
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for(NSInteger num = 0; num < Fruit_Num; num++)
    {
        NSString *str = [NSString stringWithFormat:@"%@%.2d.png",Fruit_Name,num + 1];
        FruitSprite *sprite = [FruitSprite spriteWithFile:str];
        [sprite setFramePoint:num];
        [array addObject:sprite];
    }
    return [array autorelease];
}

- (void)fruitsRange:(NSInteger)_number
{
    if([fruitsAry count])
    {
        [fruitsAry removeAllObjects];
    }
    
    NSMutableArray *tmpAry = [self getFruitsArray];
    for(NSInteger num = 0; num < _number; num++)
    {
        NSInteger randNum = arc4random()%[tmpAry count];
        FruitSprite *object = (FruitSprite*)[tmpAry objectAtIndex:randNum];
        [object setFramePoint:num];
        [fruitsAry addObject:object];
        [tmpAry removeObject:object];
    }
}

- (BOOL)fruitsCompare:(FruitSprite *)sprite;
{
    BOOL isTrue = true;
    NSInteger count = [fruitsAry count];
    FruitSprite *findSprite = NULL;
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *tmpSprite = (FruitSprite*)[fruitsAry objectAtIndex:num];
        if([tmpSprite framePoint] == [sprite framePoint])
        {
            findSprite = tmpSprite;
        }
    }
    
    if(findSprite)
    {
        if([findSprite texture] != [sprite texture])
        {
            isTrue = false;
        }
    }
    else
    {
        isTrue = false;
    }
    
    return isTrue;
}

@end
