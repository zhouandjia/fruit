//
//  GameSingleton.h
//  FruitGames
//
//  Created by Static Ga on 12-11-28.
//
//

#import <Foundation/Foundation.h>
#import "SinaWeibo.h"
@interface GameSingleton : NSObject
+ (SinaWeibo *)sharedSina;
+ (void)removeAuthData;
+ (void)storeAuthData;
@end
