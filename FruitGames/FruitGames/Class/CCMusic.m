//
//  CCMusic.m
//  FruitGames
//
//  Created by Static Ga on 12-12-28.
//
//

#import "CCMusic.h"
#import "SimpleAudioEngine.h"
#import "CCFruitsData.h"

@implementation CCMusic
//对比画面声音.mp3
+ (void)gameCompareLayerMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"对比画面声音.mp3"];
}
//放置水果后的声音.mp3
+ (void)gamePutFruitsMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"放置水果后的声音.mp3"];
}
//过场动画声音每个过场播放一次和文字一起出现.mp3
+ (void)gameLayerTransMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"过场动画声音每个过场播放一次和文字一起出现.mp3"];
}

//过关后的声音.mp3
+ (void)gameSucessMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"过关后的声音.mp3"];
}

//使用帮助后水果图片翻转的声音.mp3
+ (void)gameUserHelpMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"使用帮助后水果图片翻转的声音.mp3"];
}

//游戏开始loop.mp3
+ (void)gameStartMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"游戏开始loop.mp3"];
}

//游戏失败声音.mp3
+ (void)gameFailedMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"游戏失败声音.mp3"];
}

//Menu_Click06.mp3
+ (void)gameMenuClickMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"Menu_Click06.mp3"];
}
//点击确认的声音.mp3
+ (void)gameClickSureMusic {
    if (![CCFruitsData getSoundSettings]) {
        return;
    }
    [[SimpleAudioEngine sharedEngine] playEffect:@"点击确认的声音.mp3"];
}
@end
