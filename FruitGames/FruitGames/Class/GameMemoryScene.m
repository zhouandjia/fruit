//
//  GameMemoryScene.m
//  FruitGames
//
//  Created by 周凯俊 on 12-11-18.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "GameMemoryScene.h"
#import "CCFruits.h"
#import "FruitSprite.h"
#import "GamePlayScene.h"

#define Background @"Memory_Background.png"
#define Frame @"Memory_Frame.png"
#define Touch_Menu @"Memory_Touch_Menu.png"
#define Sprite @"guaidanshou.png"
#define Cue @"tishi1.png"
#define Cue_Two @"tishi2.png"

#define Sprite_Tag 1
#define FrameSprite_Tag 2
#define TouchMenu_Tag 3
#define DialogueSprite_Tag 4
//#define Move_Time 1.5
#define Move_Time 0.8

@implementation GameMemoryScene

+ (CCScene *)scene
{
    CCScene *scene = [CCScene node];
    GameMemoryScene *layer = [GameMemoryScene node];
    [scene addChild:layer];
    return scene;
}

- (void)dealloc
{
    [super dealloc];
}

- (id)init
{
    if(self = [super init])
    {
        [self setIsTouchEnabled:YES];
        
        [self addUI];
    }
    
    return self;
}

- (void)addUI
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *background = [CCSprite spriteWithFile:Background];
    [background setPosition:CGPointMake(winSize.width/2, winSize.height/2)];
    [self addChild:background];
    
    CCSprite *sprite = [CCSprite spriteWithFile:Sprite];
    CGSize cttSize = [sprite contentSize];
    [sprite setPosition:CGPointMake(- cttSize.width/2, cttSize.height/2)];
    [sprite setTag:Sprite_Tag];
    [self addChild:sprite];
    
    CCSprite *frameSprite = [CCSprite spriteWithFile:Frame];
    cttSize = [frameSprite contentSize];
    [frameSprite setPosition:CGPointMake(winSize.width + cttSize.width/2, cttSize.height/2)];
    [frameSprite setTag:FrameSprite_Tag];
    [self addChild:frameSprite];
    
    CCSprite *touchSprite = [CCSprite spriteWithFile:Touch_Menu];
    [touchSprite setPosition:CGPointMake(cttSize.width/5*4, cttSize.height/4)];
    [touchSprite setTag:TouchMenu_Tag];
    [frameSprite addChild:touchSprite];
    
    [self spriteMoveIn];
}

- (void)spriteMoveIn
{
    CCSprite *sprite = (CCSprite*)[self getChildByTag:Sprite_Tag];
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:FrameSprite_Tag];
    
    CGSize cttSize = [sprite contentSize];
    CCMoveBy *moveAct2 = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(cttSize.width*10/9 + 30, 0)];
    CCMoveBy *moveAct = [CCMoveBy actionWithDuration:0.1 position:CGPointMake(-30, 0)];
    CCCallFunc *callAct = [CCCallFunc actionWithTarget:self selector:@selector(dialogueShow)];
    [sprite runAction:[CCSequence actions:moveAct2, moveAct,callAct, nil]];

//    CCMoveBy *moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(cttSize.width*10/9, 0)];
//    CCCallFunc *callAct = [CCCallFunc actionWithTarget:self selector:@selector(dialogueShow)];
//    [sprite runAction:[CCSequence actions:moveAct, callAct, nil]];
    
    cttSize = [frameSprite contentSize];
    moveAct = [CCMoveBy actionWithDuration:0.1 position:CGPointMake(30, 0)];
    moveAct2 = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(- cttSize.width - 30, 0)];

    [frameSprite runAction:[CCSequence actions:moveAct2,moveAct ,nil]];
}

- (void)dialogueShow
{
    CCSprite *sprite = (CCSprite*)[self getChildByTag:Sprite_Tag];
    CGSize cttSize = [sprite contentSize];
    
    CCSprite *dialogue = [CCSprite spriteWithFile:Cue];
    [dialogue setPosition:CGPointMake(cttSize.width/2, cttSize.height + dialogue.contentSize.height/2)];
    [dialogue setTag:DialogueSprite_Tag];
    [sprite addChild:dialogue];
}

- (void)dialogueChange
{
    CCSprite *dialogue = (CCSprite*)[[self getChildByTag:Sprite_Tag] getChildByTag:DialogueSprite_Tag];
    [dialogue setTexture:[[CCTextureCache sharedTextureCache] addImage:Cue_Two]];
    
    [self fruitShow];
}

- (void)fruitShow
{
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:FrameSprite_Tag];
    CCFruits *fruits = [CCFruits shared];
    [fruits fruitsRange];
    NSMutableArray *array = [fruits getFruits];
    NSInteger count = [array count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[array objectAtIndex:num];
        [sprite setPosition:[self getPointFromFramePoint:[sprite framePoint]]];
        [frameSprite addChild:sprite];
    }
}

- (void)spriteMoveOut
{
    isMoveOut = YES;
    
    CCSprite *sprite = (CCSprite*)[self getChildByTag:Sprite_Tag];
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:FrameSprite_Tag];
    
    CGSize cttSize = [sprite contentSize];
    CCMoveBy *moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(- cttSize.width*10/9, 0)];
    CCCallFunc *callAct = [CCCallFunc actionWithTarget:self selector:@selector(spriteMoveFinish)];
    [sprite runAction:[CCSequence actions:moveAct, callAct, nil]];
    
    cttSize = [frameSprite contentSize];
    moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(cttSize.width, 0)];
    [frameSprite runAction:moveAct];
}

- (void)spriteMoveFinish
{
    [[CCDirector sharedDirector] replaceScene:[GamePlayScene scene]];
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!isTouch && !isMoveOut)
    {
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInView:[touch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        
        CCSprite *touchSprite = (CCSprite*)[[self getChildByTag:FrameSprite_Tag] getChildByTag:TouchMenu_Tag];
        CGRect rect;
        rect.origin = CGPointZero;
        rect.size = [touchSprite contentSize];
        location = [touchSprite convertToNodeSpace:location];
        if(CGRectContainsPoint(rect, location))
        {
            [self dialogueChange];
            isTouch = YES;
        }
    }
}

- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isTouch && !isMoveOut)
    {
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInView:[touch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        
        CCSprite *touchSprite = (CCSprite*)[[self getChildByTag:FrameSprite_Tag] getChildByTag:TouchMenu_Tag];
        CGRect rect;
        rect.origin = CGPointZero;
        rect.size = [touchSprite contentSize];
        location = [touchSprite convertToNodeSpace:location];
        if(!CGRectContainsPoint(rect, location))
        {
            [self spriteMoveOut];
        }
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isTouch && !isMoveOut)
    {
        [self spriteMoveOut];
    }
}

- (CGPoint)getPointFromFramePoint:(FramePoint)_framePoint
{
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:FrameSprite_Tag];
    CGSize cttSize = [frameSprite contentSize];
    
    CGPoint point;
    switch(_framePoint)
    {
        case 0:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*16);
            break;
        }
        case 1:
        {
            point = CGPointMake(cttSize.width/20*8, cttSize.height/80*16);
            break;
        }
        case 2:
        {
            point = CGPointMake(cttSize.width/20*11, cttSize.height/80*16);
            break;
        }
        case 3:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*29);
            break;
        }
        case 4:
        {
            point = CGPointMake(cttSize.width/20*8, cttSize.height/80*29);
            break;
        }
        case 5:
        {
            point = CGPointMake(cttSize.width/20*11, cttSize.height/80*29);
            break;
        }
        case 6:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*42);
            break;
        }
        case 7:
        {
            point = CGPointMake(cttSize.width/20*8, cttSize.height/80*42);
            break;
        }
        case 8:
        {
            point = CGPointMake(cttSize.width/20*11, cttSize.height/80*42);
            break;
        }
        case 9:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*54);
            break;
        }
        case 10:
        {
            point = CGPointMake(cttSize.width/20*8, cttSize.height/80*54);
            break;
        }
        case 11:
        {
            point = CGPointMake(cttSize.width/20*11, cttSize.height/80*54);
            break;
        }
        case 12:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*67);
            break;
        }
        case 13:
        {
            point = CGPointMake(cttSize.width/20*8, cttSize.height/80*67);
            break;
        }
        case 14:
        {
            point = CGPointMake(cttSize.width/20*11, cttSize.height/80*67);
            break;
        }
    }
    return point;
}

@end
