//
//  GamePlayScene.m
//  FruitGames
//
//  Created by 周凯俊 on 12-11-25.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "GamePlayScene.h"
#import "HelpNumSprite.h"
#import "FruitSprite.h"
#import "CCFruits.h"
#import "GameCompareScene.h"
#import "CCFruitsData.h"
#import "GameShopLayer.h"
#import "CCMusic.h"
#import "AppDelegate.h"

#define Background @"Memory_Background.png"
#define Left_Frame @"caozuolan.png"
#define Right_Frame @"bingxiang.png"

#define Help_Menu @"fangdajing.png"
#define Help_Menu_Selected @"fangdajing.png"
#define Finish_Menu @"querenanniu.png"
#define Finish_Menu_Selected @"querenanniu.png"
#define Pause_Menu @"zantinganniu.png"
#define Pause_Menu_Selected @"zantinganniu.png"

#define Restart_Menu @"restartBtn.png"
#define Restart_Menu_Selected @"restartBtnPressed.png"

#define Left_Frame_Tag 1
#define Right_Frame_Tag 2
#define Help_Num_Tag 3
#define Move_Time 1.5

#define Fruit_Num 15
#define Fruit_ZOrder 10

#define Change_Time 0.25

@implementation GamePlayScene

+ (CCScene *)scene
{
    CCScene *scene = [CCScene node];
    GamePlayScene *layer = [GamePlayScene node];
    [scene addChild:layer];
    return scene;
}

- (void)dealloc
{
    [showArray release];
    showArray = NULL;
    [putArray release];
    putArray = NULL;
    [super dealloc];
}

- (id)init
{
    if(self = [super init])
    {
        AppController *controller = (AppController *)[UIApplication sharedApplication].delegate;
        [controller clearAd];

        [self setIsTouchEnabled:YES];
        
        showArray = [[NSMutableArray alloc] init];
        putArray = [[NSMutableArray alloc] init];
        
        [self addUI];
    }
    
    return self;
}

- (void)addUI
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *background = [CCSprite spriteWithFile:Background];
    [background setPosition:CGPointMake(winSize.width/2, winSize.height/2)];
    [self addChild:background];
    
    CCSprite *rightSprite = [CCSprite spriteWithFile:Right_Frame];
    CGSize cttSize = [rightSprite contentSize];
    [rightSprite setPosition:CGPointMake(winSize.width + cttSize.width/2, cttSize.height/2)];
    [rightSprite setTag:Right_Frame_Tag];
    [self addChild:rightSprite];
    
    CCMoveBy *moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(- cttSize.width, 0)];
    [rightSprite runAction:moveAct];
    
    CCSprite *leftSprite = [CCSprite spriteWithFile:Left_Frame];
    cttSize = [leftSprite contentSize];
    [leftSprite setPosition:CGPointMake(- cttSize.width/2, cttSize.height/2)];
    [leftSprite setTag:Left_Frame_Tag];
    [self addChild:leftSprite];
    
    [self addMenu];
    [self addFruits];
    
    moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(cttSize.width, 0)];
    [leftSprite runAction:moveAct];
}

- (void)addMenu
{
    CCSprite *sprite = (CCSprite*)[self getChildByTag:Left_Frame_Tag];
    CGSize cttSize = [sprite contentSize];
    
    CCMenuItemImage *pauseItem = [CCMenuItemImage itemWithNormalImage:Pause_Menu selectedImage:Pause_Menu_Selected target:self selector:@selector(pauseMenu)];
    [pauseItem setPosition:CGPointMake(cttSize.width/5, cttSize.height/4*3)];
    
    CCMenuItemImage *helpItem = [CCMenuItemImage itemWithNormalImage:Help_Menu selectedImage:Help_Menu_Selected target:self selector:@selector(helpMenu:)];
    [helpItem setPosition:CGPointMake(cttSize.width/5, cttSize.height/2)];
    
    CCMenuItemImage *finishItem = [CCMenuItemImage itemWithNormalImage:Finish_Menu selectedImage:Finish_Menu_Selected target:self selector:@selector(finishMenu)];
    [finishItem setPosition:CGPointMake(cttSize.width/5, cttSize.height/4)];
    
    CCMenu *menu = [CCMenu menuWithItems:pauseItem, helpItem, finishItem, nil];
    [menu setPosition:CGPointZero];
    [sprite addChild:menu];
    
    HelpNumSprite *helpNumSprite = [HelpNumSprite sprite];
    cttSize = [helpItem contentSize];
    [helpNumSprite setPosition:CGPointMake(cttSize.width, cttSize.height)];
    [helpNumSprite setTag:Help_Num_Tag];
    [helpItem addChild:helpNumSprite];
}

- (void)addFruits
{
    CGSize cttSize = [[self getChildByTag:Left_Frame_Tag] contentSize];
    NSMutableArray *array = [[CCFruits shared] getFruitsArray];
    for(NSInteger num = 0; num < Fruit_Num; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[array objectAtIndex:num];
        [sprite setFramePoint:num + Fruit_Num];
        [sprite setPosition:[self getPointFromFramePoint:[sprite framePoint]]];
        [self addChild:sprite z:Fruit_ZOrder];
        
        CCMoveBy *moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(cttSize.width, 0)];
        [sprite runAction:moveAct];
        
        [showArray addObject:sprite];
    }
}

- (void)pauseMenu
{
    [CCMusic gameMenuClickMusic];
    [[CCDirector sharedDirector] pushScene:[GameShopLayer sceneWithBackType:kBackPlayMenuType]];
    
}

- (void)helpMenu:(id)sender
{
    [CCMusic gameMenuClickMusic];
    NSInteger helpNum = [CCFruitsData getDJNum];
    NSLog(@"getDJNum %i",helpNum);
    if(helpNum <= 0)
    {
        return;
    }
    else
    {
        [CCFruitsData saveDJNum:--helpNum];
        CCNode *node = (CCNode*)sender;
        [node removeChildByTag:Help_Num_Tag cleanup:true];

        CGSize cttSize = [node contentSize];
        HelpNumSprite *helpNumSprite = [HelpNumSprite sprite];
        [helpNumSprite setPosition:CGPointMake(cttSize.width, cttSize.height)];
        [helpNumSprite setTag:Help_Num_Tag];
        [node addChild:helpNumSprite];
    }
    
    NSMutableArray *array = [[CCFruits shared] getFruits];
    NSInteger count = [array count];
    NSInteger tmpCount = [putArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *suredSprite = (FruitSprite*)[array objectAtIndex:num];
        FruitSprite *changeSprite = NULL;
        BOOL canChange = true;
        for(NSInteger tmpNum = 0; tmpNum < tmpCount; tmpNum++)
        {
            FruitSprite *sprite = (FruitSprite*)[putArray objectAtIndex:tmpNum];
            if([suredSprite framePoint] == [sprite framePoint])
            {
                if([sprite texture] != [suredSprite texture])
                {
                    changeSprite = sprite;
                    break;
                }
                else
                {
                    canChange = false;
                }
            }
        }
        
        if(canChange)
        {
            [self setSprite:[self findFruitSprite:suredSprite] ToFramePoint:num Action:true];
            break;
        }
    }
}

- (FruitSprite*)findFruitSprite:(FruitSprite *)sprite
{
    FruitSprite *fruitSprite = NULL;
    NSInteger count = [showArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *tmpSprite = (FruitSprite*)[showArray objectAtIndex:num];
        if([sprite texture] == [tmpSprite texture])
        {
            fruitSprite = tmpSprite;
            break;
        }
    }
    
    count = [putArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *tmpSprite = (FruitSprite*)[putArray objectAtIndex:num];
        if([sprite texture] == [tmpSprite texture])
        {
            fruitSprite = tmpSprite;
            break;
        }
    }
    
    return fruitSprite;
}

- (void)finishMenu
{
    [CCMusic gameMenuClickMusic];
    [self fruitsMoveAway];
    
    CCDelayTime *delayAct = [CCDelayTime actionWithDuration:Move_Time];
    CCCallFunc *callAct = [CCCallFunc actionWithTarget:self selector:@selector(changeScene)];
    [self runAction:[CCSequence actions:delayAct, callAct, nil]];
}

- (void)changeScene
{
    [[CCDirector sharedDirector] replaceScene:[GameCompareScene scene:putArray]];
}

- (void)fruitsMoveAway
{
    CCSprite *leftFrame = (CCSprite*)[self getChildByTag:Left_Frame_Tag];
    CGSize cttSize = [leftFrame contentSize];
    CCMoveBy *moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(- cttSize.width, 0)];
    [leftFrame runAction:moveAct];
    
    NSInteger count = [showArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[showArray objectAtIndex:num];
        moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(- cttSize.width, 0)];
        [sprite runAction:moveAct];
    }
    
    CCSprite *rightFrame = (CCSprite*)[self getChildByTag:Right_Frame_Tag];
    cttSize = [rightFrame contentSize];
    moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(cttSize.width, 0)];
    [rightFrame runAction:moveAct];
    
    count = [putArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[putArray objectAtIndex:num];
        moveAct = [CCMoveBy actionWithDuration:Move_Time position:CGPointMake(cttSize.width, 0)];
        [sprite runAction:moveAct];
    }
}

- (CGPoint)getLeftPointFromFramePoint:(FramePoint)_framePoint
{
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:Left_Frame_Tag];
    CGSize cttSize = [frameSprite contentSize];
    
    CGPoint point;
    switch(_framePoint)
    {
        case 0:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*14);
            break;
        }
        case 1:
        {
            point = CGPointMake(cttSize.width/20*12, cttSize.height/80*14);
            break;
        }
        case 2:
        {
            point = CGPointMake(cttSize.width/20*15, cttSize.height/80*14);
            break;
        }
        case 3:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*27);
            break;
        }
        case 4:
        {
            point = CGPointMake(cttSize.width/20*12, cttSize.height/80*27);
            break;
        }
        case 5:
        {
            point = CGPointMake(cttSize.width/20*15, cttSize.height/80*27);
            break;
        }
        case 6:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*40);
            break;
        }
        case 7:
        {
            point = CGPointMake(cttSize.width/20*12, cttSize.height/80*40);
            break;
        }
        case 8:
        {
            point = CGPointMake(cttSize.width/20*15, cttSize.height/80*40);
            break;
        }
        case 9:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*52);
            break;
        }
        case 10:
        {
            point = CGPointMake(cttSize.width/20*12, cttSize.height/80*52);
            break;
        }
        case 11:
        {
            point = CGPointMake(cttSize.width/20*15, cttSize.height/80*52);
            break;
        }
        case 12:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*65);
            break;
        }
        case 13:
        {
            point = CGPointMake(cttSize.width/20*12, cttSize.height/80*65);
            break;
        }
        case 14:
        {
            point = CGPointMake(cttSize.width/20*15, cttSize.height/80*65);
            break;
        }
    }
    CGPoint position = [frameSprite position];
    point.x = position.x - cttSize.width/2 + point.x;
    point.y = position.y - cttSize.height/2 + point.y;
    return point;
}

- (CGPoint)getRightPointFromFramePoint:(FramePoint)_framePoint
{
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:Right_Frame_Tag];
    CGSize cttSize = [frameSprite contentSize];
    
    CGPoint point;
    switch(_framePoint)
    {
        case 0:
        {
            point = CGPointMake(cttSize.width/4, cttSize.height/80*15);
            break;
        }
        case 1:
        {
            point = CGPointMake(cttSize.width/2, cttSize.height/80*15);
            break;
        }
        case 2:
        {
            point = CGPointMake(cttSize.width/4*3, cttSize.height/80*15);
            break;
        }
        case 3:
        {
            point = CGPointMake(cttSize.width/4, cttSize.height/80*28);
            break;
        }
        case 4:
        {
            point = CGPointMake(cttSize.width/2, cttSize.height/80*28);
            break;
        }
        case 5:
        {
            point = CGPointMake(cttSize.width/4*3, cttSize.height/80*28);
            break;
        }
        case 6:
        {
            point = CGPointMake(cttSize.width/4, cttSize.height/80*41);
            break;
        }
        case 7:
        {
            point = CGPointMake(cttSize.width/2, cttSize.height/80*41);
            break;
        }
        case 8:
        {
            point = CGPointMake(cttSize.width/4*3, cttSize.height/80*41);
            break;
        }
        case 9:
        {
            point = CGPointMake(cttSize.width/4, cttSize.height/80*54);
            break;
        }
        case 10:
        {
            point = CGPointMake(cttSize.width/2, cttSize.height/80*54);
            break;
        }
        case 11:
        {
            point = CGPointMake(cttSize.width/4*3, cttSize.height/80*54);
            break;
        }
        case 12:
        {
            point = CGPointMake(cttSize.width/4, cttSize.height/80*67);
            break;
        }
        case 13:
        {
            point = CGPointMake(cttSize.width/2, cttSize.height/80*67);
            break;
        }
        case 14:
        {
            point = CGPointMake(cttSize.width/4*3, cttSize.height/80*67);
            break;
        }
    }
    CGPoint position = [frameSprite position];
    point.x = position.x - cttSize.width/2 + point.x;
    point.y = position.y - cttSize.height/2 + point.y;
    return point;
}

- (CGPoint)getPointFromFramePoint:(FramePoint)_framePoint
{
    if(_framePoint < Fruit_Num)
    {
        return [self getRightPointFromFramePoint:_framePoint];
    }
    else
    {
        return [self getLeftPointFromFramePoint:_framePoint%Fruit_Num];
    }
}

- (void)setSprite:(FruitSprite*)_moveSprite ToFramePoint:(FramePoint)_framePoint Action:(BOOL)_act
{
    [CCMusic gameUserHelpMusic];
    FruitSprite *changeSprite = NULL;
    NSInteger count = [showArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[showArray objectAtIndex:num];
        if([sprite framePoint] == _framePoint)
        {
            changeSprite = sprite;
            break;
        }
    }
    
    count = [putArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[putArray objectAtIndex:num];
        if([sprite framePoint] == _framePoint)
        {
            changeSprite = sprite;
            break;
        }
    }
    
    if(changeSprite)
    {
        NSInteger tmp = [_moveSprite framePoint];
        [_moveSprite setFramePoint:[changeSprite framePoint]];
        [changeSprite setFramePoint:tmp];
        
        if(_act)
        {
            CCScaleTo *scaleAct = [CCScaleTo actionWithDuration:Change_Time scaleX:0 scaleY:1];
            CCScaleTo *rescaleAct = [CCScaleTo actionWithDuration:Change_Time scaleX:1 scaleY:1];
            CCCallBlock *callAct = [CCCallBlock actionWithBlock:^(void){ [_moveSprite setPosition:[self getPointFromFramePoint:[_moveSprite framePoint]]]; }];
            [_moveSprite runAction:[CCSequence actions:scaleAct, callAct, rescaleAct, nil]]
            ;
            
            CCScaleTo *ascaleAct = [CCScaleTo actionWithDuration:Change_Time scaleX:0 scaleY:1];
            CCScaleTo *arescaleAct = [CCScaleTo actionWithDuration:Change_Time scaleX:1 scaleY:1];
            CCCallBlock *acallAct = [CCCallBlock actionWithBlock:^(void){ [changeSprite setPosition:[self getPointFromFramePoint:[changeSprite framePoint]]]; }];
            [changeSprite runAction:[CCSequence actions:ascaleAct, acallAct, arescaleAct, nil]];
        }
        else
        {
            [_moveSprite setPosition:[self getPointFromFramePoint:[_moveSprite framePoint]]];
            [changeSprite setPosition:[self getPointFromFramePoint:[changeSprite framePoint]]];
        }
    }
    else
    {
        [_moveSprite setFramePoint:_framePoint];
        if(_act)
        {
            CCScaleTo *scaleAct = [CCScaleTo actionWithDuration:Change_Time scaleX:0 scaleY:1];
            CCScaleTo *rescaleAct = [CCScaleTo actionWithDuration:Change_Time scaleX:1 scaleY:1];
            CCCallBlock *callAct = [CCCallBlock actionWithBlock:^(void){ [_moveSprite setPosition:[self getPointFromFramePoint:[_moveSprite framePoint]]]; }];
            [_moveSprite runAction:[CCSequence actions:scaleAct, callAct, rescaleAct, nil]]
            ;
        }
        else
        {
            [_moveSprite setPosition:[self getPointFromFramePoint:[_moveSprite framePoint]]];
        }
    }
    
    if([showArray containsObject:_moveSprite])
    {
        if(_framePoint < Fruit_Num)
        {
            [putArray addObject:_moveSprite];
            [showArray removeObject:_moveSprite];
            if(changeSprite)
            {
                [showArray addObject:changeSprite];
                [putArray removeObject:changeSprite];
            }
        }
    }
    else
    {
        if(_framePoint >= Fruit_Num)
        {
            [showArray addObject:_moveSprite];
            [putArray removeObject:_moveSprite];
            if(changeSprite)
            {
                [putArray addObject:changeSprite];
                [showArray removeObject:changeSprite];
            }
        }
    }
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    NSInteger count = [showArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[showArray objectAtIndex:num];
        CGRect rect;
        rect.origin = CGPointZero;
        rect.size = [sprite contentSize];
        CGPoint point = [sprite convertToNodeSpace:location];
        if(CGRectContainsPoint(rect, point))
        {
            moveSprite = sprite;
            beginPoint = location;
            return;
        }
    }
    
    count = [putArray count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[putArray objectAtIndex:num];
        CGRect rect;
        rect.origin = CGPointZero;
        rect.size = [sprite contentSize];
        CGPoint point = [sprite convertToNodeSpace:location];
        if(CGRectContainsPoint(rect, point))
        {
            moveSprite = sprite;
            beginPoint = location;
            return;
        }
    }
}

- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(moveSprite)
    {
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInView:[touch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        
        CGPoint position = [moveSprite position];
        CGPoint newPos;
        newPos.x = location.x - beginPoint.x + position.x;
        newPos.y = location.y - beginPoint.y + position.y;
        [moveSprite setPosition:newPos];
        beginPoint = location;
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [CCMusic gamePutFruitsMusic];
    if(moveSprite)
    {
        BOOL isBack = true;
        
        CGSize cttSize = [moveSprite contentSize];
        for(NSInteger num = 0; num < Fruit_Num*2; num++)
        {
            CGPoint location = [self getPointFromFramePoint:num];
            CGRect rect = CGRectMake(location.x - cttSize.width/2, location.y - cttSize.height/2, cttSize.width, cttSize.height);
            if(CGRectContainsPoint(rect, [moveSprite position]))
            {
                isBack = false;
                [self setSprite:moveSprite ToFramePoint:num Action:false];
                break;
            }
        }
        
        if(isBack)
        {
            [moveSprite setPosition:[self getPointFromFramePoint:[moveSprite framePoint]]];
        }
        
        moveSprite = NULL;
    }
}

@end
