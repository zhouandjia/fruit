//
//  CCMusic.h
//  FruitGames
//
//  Created by Static Ga on 12-12-28.
//
//

#import <Foundation/Foundation.h>

@interface CCMusic : NSObject
//对比画面声音.mp3
+ (void)gameCompareLayerMusic;
//放置水果后的声音.mp3
+ (void)gamePutFruitsMusic;
//过场动画声音每个过场播放一次和文字一起出现.mp3
+ (void)gameLayerTransMusic;
//过关后的声音.mp3
+ (void)gameSucessMusic;
//使用帮助后水果图片翻转的声音.mp3
+ (void)gameUserHelpMusic;
//游戏开始loop.mp3
+ (void)gameStartMusic;
//游戏失败声音.mp3
+ (void)gameFailedMusic;
//Menu_Click06.mp3
+ (void)gameMenuClickMusic;
//点击确认的声音.mp3
+ (void)gameClickSureMusic;
@end
