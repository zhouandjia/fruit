//
//  GameShopLayer.m
//  FruitGames
//
//  Created by Static Ga on 12-12-5.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "GameShopLayer.h"
#import "GameMenuScene.h"
#import "GameResultScene.h"
#import "GamePlayScene.h"
#import "GameMemoryScene.h"
#import "CCMusic.h"
#import "CCFruitsData.h"
#import "AppDelegate.h"

#define GS_BG @"shangdiantongyong.png"
#define GS_BackNormal @"fanhui.png"
#define GS_BackPressed @"fanhuiPressed.png"
#define GS_PayNormal @"fukuan.png"
#define GS_PayPressed @"fukuanPressed.png"
#define GS_MultiplyFlag @"X.png"
#define GS_MoneyFlag @"¥.png"
#define GS_FangDajing @"fangdajing.png"
#define GS_Sum @"=.png"

#define GS_RestartNormal @"restartBtn.png"
#define GS_RestartPressed @"restartBtnPressed.png"

#define ProductID_IAP18 @"buzzstudioIAP006"//$18
#define ProductID_IAP12 @"buzzstudioIAP005" //$12
#define ProductID_IAP6 @"buzzstudioIAP004" //$6

#define PayTag 3001
@interface GameShopLayer ()
@property (nonatomic, assign) kBackMenuType backType;
@end

@implementation GameShopLayer
+ (CCScene *)sceneWithBackType:(kBackMenuType)type
 {
    CCScene *scene = [CCScene node];
    GameShopLayer *layer = [[[GameShopLayer alloc] initWithType:type] autorelease];
    [scene addChild:layer];
    return scene;
}

- (id)initWithType: (kBackMenuType)type {
    if (self = [super init]) {
        self.backType = type;
        [self addUI];
        
        AppController *controller = (AppController *)[UIApplication sharedApplication].delegate;
        [controller startLoadAd];

        CGSize winSize = [[CCDirector sharedDirector] winSize];
        if (kBackPlayMenuType == type) {
            CCMenuItemImage *restartMenu = [CCMenuItemImage itemWithNormalImage:GS_RestartNormal selectedImage:GS_RestartPressed target:self selector:@selector(restartGames)];
            restartMenu.position = CGPointMake(winSize.width - restartMenu.contentSize.width, restartMenu.contentSize.height/2 + restartMenu.contentSize.height/3);
            CCMenu *menu = [CCMenu menuWithItems:restartMenu, nil];
            menu.position = CGPointZero;
            [self addChild:menu];
            
            //----监听购买结果
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            
        }
    }
    return self;
}

- (void)addUI {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCSprite *bgSprite = [CCSprite spriteWithFile:GS_BG];
    bgSprite.position = ccp(winSize.width/2, winSize.height/2);
    [self addChild:bgSprite];
    
    CGFloat centerY = 0;
    for (int i = 0 ;i < 3; i++) {
        CCSprite *fangdajingSprite = [CCSprite spriteWithFile:GS_FangDajing];
        CGFloat dis = 0;
        if (0 == i) {
            dis = 10;
        }else {
            dis = -fangdajingSprite.contentSize.height/3;
        }
        centerY +=  fangdajingSprite.contentSize.height + fangdajingSprite.contentSize.height/2 + dis;
        fangdajingSprite.position = ccp(fangdajingSprite.contentSize.width*2, centerY);
        [self addChild:fangdajingSprite];
        
        CCSprite *multiplySprite = [CCSprite spriteWithFile:GS_MultiplyFlag];
        multiplySprite.position = ccp(fangdajingSprite.position.x + fangdajingSprite.contentSize.width, fangdajingSprite.position.y);
        [self addChild:multiplySprite];
        
        CCSprite *numberSprite = nil;
        CGFloat numX = multiplySprite.position.x + multiplySprite.contentSize.width;
        NSString *totalMoney = nil;
        NSMutableArray *totolMoneyArray = [NSMutableArray array];
        switch (i) {
            case 0:
            {
                numberSprite = [CCSprite spriteWithFile:@"2.png"];
                numberSprite.position = ccp(numX, multiplySprite.position.y);
                [self addChild:numberSprite];
                
                numberSprite = [CCSprite spriteWithFile:@"0.png"];
                numberSprite.position = ccp(numX + numberSprite.contentSize.width, multiplySprite.position.y);
                [self addChild:numberSprite];
                
                totalMoney = @"6.png";
                [totolMoneyArray addObject:@"6.png"];
    
            }
                break;
            case 1:
            {
                numberSprite = [CCSprite spriteWithFile:@"5.png"];
                numberSprite.position = ccp(numX, multiplySprite.position.y);
                [self addChild:numberSprite];
                
                numberSprite = [CCSprite spriteWithFile:@"0.png"];
                numberSprite.position = ccp(numX + numberSprite.contentSize.width, multiplySprite.position.y);
                [self addChild:numberSprite];

                totalMoney = @"12.png";
                [totolMoneyArray addObject:@"1.png"];
                [totolMoneyArray addObject:@"2.png"];
            }
                break;
            case 2:
            {
                numberSprite = [CCSprite spriteWithFile:@"1.png"];
                numberSprite.position = ccp(numX, multiplySprite.position.y);
                [self addChild:numberSprite];
                
                numberSprite = [CCSprite spriteWithFile:@"2.png"];
                numberSprite.position = ccp(numX + numberSprite.contentSize.width, multiplySprite.position.y);
                [self addChild:numberSprite];
                
                numberSprite = [CCSprite spriteWithFile:@"0.png"];
                numberSprite.position = ccp(numX + 2*numberSprite.contentSize.width, multiplySprite.position.y);
                [self addChild:numberSprite];

                totalMoney = @"18.png";
                [totolMoneyArray addObject:@"1.png"];
                [totolMoneyArray addObject:@"8.png"];

            }
                break;
            default:
                break;
        }
        
        CCSprite *sumSprite = [CCSprite spriteWithFile:GS_Sum];
        sumSprite.position = ccp(multiplySprite.position.x + 3*multiplySprite.contentSize.width, multiplySprite.position.y);
        [self addChild:sumSprite];
        
        CCSprite *moneyFlag = [CCSprite spriteWithFile:GS_MoneyFlag];
        moneyFlag.position = ccp(sumSprite.position.x + moneyFlag.contentSize.width, sumSprite.position.y);
        [self addChild:moneyFlag];
        
        CGFloat moneyX = moneyFlag.position.x + moneyFlag.contentSize.width;
        for (int i = 0; i < totolMoneyArray.count; i++) {
            CCSprite *totalSprite = [CCSprite spriteWithFile:[totolMoneyArray objectAtIndex:i]];
            totalSprite.position = ccp(moneyX, moneyFlag.position.y);
            [self addChild:totalSprite];
            moneyX += 2*totalSprite.contentSize.width;
        }
        
        CCMenuItemImage *paySprite = [CCMenuItemImage itemWithNormalImage:GS_PayNormal selectedImage:GS_PayPressed target:self selector:@selector(payfor:)];
        paySprite.tag = PayTag + i;
        paySprite.position = ccp( moneyFlag.position.x + moneyFlag.contentSize.width + paySprite.contentSize.width, moneyFlag.position.y);
        CCMenu *menu = [CCMenu menuWithItems:paySprite, nil];
        menu.position = CGPointZero;
        [self addChild:menu];
        
    }
    CCMenuItemImage *back = [CCMenuItemImage itemWithNormalImage:GS_BackNormal selectedImage:GS_BackPressed target:self selector:@selector(goBack)];
    back.position = ccp(winSize.width - back.contentSize.width, winSize.height - back.contentSize.height*(3/2));
    CCMenu *menu = [CCMenu menuWithItems:back, nil];
    menu.position = CGPointZero;
    [self addChild:menu];
}

- (void)payfor :(id)sender{
    [CCMusic gameMenuClickMusic];
    if ([SKPaymentQueue canMakePayments]) {
        //[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
        [self RequestProductData];
        CCLOG(@"允许程序内付费购买");
    }
    else
    {
        CCLOG(@"不允许程序内付费购买");
        UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"You can‘t purchase in app store"
                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",nil) otherButtonTitles:nil];
        
        [alerView show];
        [alerView release];
        
    }
    CCMenuItemImage *item = (CCMenuItemImage *)sender;
    switch (item.tag - PayTag) {
        case 0:
        {
            buyType = IAP6;
        }
            break;
        case 1:
        {
            buyType = IAP12;
        }
            break;
        case 2:
        {
            buyType = IAP18;
        }
            break;
        default:
            break;
    }
}

-(void)RequestProductData
{
    CCLOG(@"---------请求对应的产品信息------------");
    NSArray *product = nil;
    switch (buyType) {
        case IAP6:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP6,nil];
            break;
        case IAP12:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP12,nil];
            break;
        case IAP18:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP18,nil];
            break;
        default:
            break;
    }
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request=[[SKProductsRequest alloc] initWithProductIdentifiers: nsset];
    request.delegate=self;
    [request start];
    [product release];
}

//<SKProductsRequestDelegate> 请求协议
//收到的产品信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    NSLog(@"-----------收到产品反馈信息--------------");
    NSArray *myProduct = response.products;
    NSLog(@"产品Product ID:%@",response.invalidProductIdentifiers);
    NSLog(@"产品付费数量: %d", [myProduct count]);
    // populate UI
    for(SKProduct *product in myProduct){
        NSLog(@"product info");
        NSLog(@"SKProduct 描述信息%@", [product description]);
        NSLog(@"产品标题 %@" , product.localizedTitle);
        NSLog(@"产品描述信息: %@" , product.localizedDescription);
        NSLog(@"价格: %@" , product.price);
        NSLog(@"Product id: %@" , product.productIdentifier);
    }
    SKPayment *payment = nil;
    switch (buyType) {
        case IAP6:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP6];    //支付$6
            break;
        case IAP12:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP12];    //支付$12
            break;
        case IAP18:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP18];    //支付$18
            break;
        default:
            break;
    }
    CCLOG(@"---------发送购买请求------------");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    [request autorelease];
    
}

//弹出错误信息
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    CCLOG(@"-------弹出错误信息----------");
    UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",NULL) message:[error localizedDescription]
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",nil) otherButtonTitles:nil];
    [alerView show];
    [alerView release];
}

-(void) requestDidFinish:(SKRequest *)request
{
    NSLog(@"----------反馈信息结束--------------");
    
}

-(void) PurchasedTransaction: (SKPaymentTransaction *)transaction{
    CCLOG(@"-----PurchasedTransaction----");
    NSArray *transactions =[[NSArray alloc] initWithObjects:transaction, nil];
    [self paymentQueue:[SKPaymentQueue defaultQueue] updatedTransactions:transactions];
    [transactions release];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions//交易结果
{
    CCLOG(@"-----paymentQueue-------- %@",transactions);
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased://交易完成
                [self completeTransaction:transaction];
                CCLOG(@"-----交易完成 --------");
                CCLOG(@"不允许程序内付费购买");
                UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"购买成功啦"
                                                                   delegate:nil cancelButtonTitle:NSLocalizedString(@"关闭",nil) otherButtonTitles:nil];
                
                [alerView show];
                [alerView release];
                [self payForSuccess];
                break;
            case SKPaymentTransactionStateFailed://交易失败
                [self failedTransaction:transaction];
                CCLOG(@"-----交易失败 --------");
                UIAlertView *alerView2 =  [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                     message:@"购买失败，请重新尝试购买～"
                                                                    delegate:nil cancelButtonTitle:NSLocalizedString(@"Close（关闭）",nil) otherButtonTitles:nil];
                
                [alerView2 show];
                [alerView2 release];
                break;
            case SKPaymentTransactionStateRestored://已经购买过该商品
                [self restoreTransaction:transaction];
                CCLOG(@"-----已经购买过该商品 --------");
            case SKPaymentTransactionStatePurchasing:      //商品添加进列表
                CCLOG(@"-----商品添加进列表 --------");
                break;
            default:
                break;
        }
    }
}

- (void)payForSuccess {
    NSInteger djNum = [CCFruitsData getDJNum];
    switch (buyType) {
        case IAP6:
            [CCFruitsData saveDJNum:(djNum + 20)];
            break;
        case IAP12:
            [CCFruitsData saveDJNum:(djNum + 50)];
            break;
        case IAP18:
            [CCFruitsData saveDJNum:(djNum + 120)];
            break;
        default:
            break;
    }

}
- (void) completeTransaction: (SKPaymentTransaction *)transaction

{
    CCLOG(@"-----completeTransaction--------");
    // Your application should implement these two methods.
    NSString *product = transaction.payment.productIdentifier;
    if ([product length] > 0) {
        
        NSArray *tt = [product componentsSeparatedByString:@"."];
        NSString *bookid = [tt lastObject];
        if ([bookid length] > 0) {
            [self recordTransaction:bookid];
            [self provideContent:bookid];
        }
    }
    
    // Remove the transaction from the payment queue.
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

//记录交易
-(void)recordTransaction:(NSString *)product{
    CCLOG(@"-----记录交易--------");
}

//处理下载内容
-(void)provideContent:(NSString *)product{
    CCLOG(@"-----下载--------");
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction{
    NSLog(@"失败");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    
}
-(void) paymentQueueRestoreCompletedTransactionsFinished: (SKPaymentTransaction *)transaction{
    
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction

{
    NSLog(@" 交易恢复处理");
    
}

-(void) paymentQueue:(SKPaymentQueue *) paymentQueue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    CCLOG(@"-------paymentQueue----");
}


#pragma mark connection delegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"%@",  [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    switch([(NSHTTPURLResponse *)response statusCode]) {
        case 200:
        case 206:
            break;
        case 304:
            break;
        case 400:
            break;
        case 404:
            break;
        case 416:
            break;
        case 403:
            break;
        case 401:
        case 500:
            break;
        default:
            break;
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"test");
}

-(void)dealloc
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];//解除监听
    [super dealloc];
}
- (void)restartGames {
    [CCMusic gameMenuClickMusic];
    [[CCDirector sharedDirector] replaceScene:[GameMemoryScene scene]];
}

- (void)goBack {
    [CCMusic gameMenuClickMusic];
    switch (self.backType) {
        case kBackMainMenuType:
        {
            [[CCDirector sharedDirector] replaceScene:[GameMenuScene scene]];

        }
            break;
        case kBackPlayMenuType:
        {
            AppController *controller = (AppController *)[UIApplication sharedApplication].delegate;
            [controller clearAd];
            [[CCDirector sharedDirector] popScene];
        }
            break;
        case kBackResultMenuType:
        {
            [[CCDirector sharedDirector] replaceScene:[GameResultScene scene:self.result]];
        }
            break;
        default:
            break;
    }
}

@end
