//
//  Contants.h
//  FruitGames
//
//  Created by Static Ga on 12-11-27.
//
//

#import <Foundation/Foundation.h>

extern NSString * const kSinaAppKey;
extern NSString * const kSinaAppSecret;
extern NSString * const kSinaSsoCallbackScheme;
extern NSString * const kSinaredirect_URI;

//userDefaults
extern NSString * const kUserDefaultsAccessTokenKey;
extern NSString * const kUserDefaultsExpirationDateKey;
extern NSString * const kUserDefaultsUserIDKey;
extern NSString * const kUserDefaultsRefreshDateKey;
extern NSString * const kUserDefaultsSinaWeiboAuthData;