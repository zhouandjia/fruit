//
//  CCFruits.h
//  FruitGames
//
//  Created by 周凯俊 on 12-11-24.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class FruitSprite;

@interface CCFruits : CCNode {
    NSMutableArray *fruitsAry;
    NSInteger index;
    NSInteger chanceNum;
}

+ (CCFruits *)shared;
- (void)clearFruits; //清除每次游戏的数据
- (NSMutableArray *)getFruits;
- (void)indexAdd; //关卡数量加一
- (NSInteger)getIndex; //获取关卡数量
- (void)chanceNumSub; //机会数量减一
- (NSInteger)getChanceNum; //获取机会数量
- (void)fruitsRange;
- (NSMutableArray *)getFruitsArray;
- (BOOL)fruitsCompare:(FruitSprite *)sprite;

@end
