//
//  GameMenuScene.m
//  FruitGames
//
//  Created by 周凯俊 on 12-11-18.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "GameMenuScene.h"
#import "GameOrdersScene.h"
#import "SinaWeibo.h"
#import "Contants.h"
#import "AppDelegate.h"
#import "GameSingleton.h"
#import "UIView+Helper.h"
#import "GameResultScene.h"
#import "GameShopLayer.h"
#import <objc/runtime.h>
#import "CCFruitsData.h"
#import "CCMusic.h"
#import "GameLoginAwardsScene.h"

#define Background @"GameMenu_Background.png"

#define Play_Menu @"Play_Menu.png"
#define Play_Menu_Selected @"Play_Menu2.png"
#define Center_Menu @"reset.png"
#define Center_Menu_Selected @"resetPressed.png"
#define Setting_Menu @"Setting_Menu.png"
#define Setting_Menu_Selected @"Setting_Menu2.png"
#define Shop_Menu @"Shop_Menu.png"
#define Shop_Menu_Selected @"Shop_Menu2.png"
#define Sina_Menu @"Sina_Menu.png"
#define Sina_Menu_Selected @"Sina_Menu2.png"

#define Game_SoundOn @"soundON.png"
#define Game_SoundOff @"soundOFF.png"
#define Game_DeveloperNormal @"developer01.png"
#define Game_DeveloperPressed @"developer02.png"
#define Game_RankingNormal @"ranking01.png"
#define Game_RankingPressed @"ranking02.png"
#define Game_AchieveNormal @"achivements01.png"
#define Game_AchievePressed @"achivements02.png"
#define Game_IconContainer @"iconBounder.png"

#define Tag_ShareSinaBg 1001
#define Tag_ShareSprite 1002
#define Tag_ShareBack 1003

#define Tag_SetMenu 1004
#define Tag_Sound 1005
#define Tag_Developer 1006

#define Tag_GameCenterMenu 1007
#define Tag_Rank 1008
#define Tag_Achieve 1009

#define Tag_ShareMenu 1010
#define Tag_Menu 1011
#define Tag_IconContainer 1012

@implementation GameMenuScene

+ (CCScene *)scene
{
    CCScene *scene = [CCScene node];
    GameMenuScene *layer = [GameMenuScene node];
    [scene addChild:layer];
    return scene;
}

- (void)dealloc
{
    [_textView release];
    _textView = nil;
    
    [_roteView release];
    _roteView = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

- (void)onEnter  //cocos2d 2.x bug 第一个界面在init里面初始化，winSize的width和height是反的，而在onEnter中是正确的，因此需要一个过渡界面
{
    [super onEnter];
    
    [self addUI];
    [self addMenu];
    [self judgeLoginAwards];
    [self addNotification];
}

- (void)addUI
{
    AppController *appDelegate = (AppController *)[UIApplication sharedApplication].delegate;
    [appDelegate clearAd];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *backgroundSprite = [CCSprite spriteWithFile:Background];
    [backgroundSprite setPosition:CGPointMake(winSize.width/2, winSize.height/2)];
    [self addChild:backgroundSprite];
}

- (void)judgeLoginAwards {
    if ([CCFruitsData shouldShowLoginAwards]) {
        [self addChild:[[GameLoginAwardsScene alloc] init]];
    }
}

- (void)addMenu
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCMenuItemImage *settingMenu = [CCMenuItemImage itemWithNormalImage:Setting_Menu selectedImage:Setting_Menu_Selected target:self selector:@selector(settingMenu)];
    [settingMenu setPosition:CGPointMake(winSize.width/7*6, winSize.height/8*7)];
    
    CCMenuItemImage *shopMenu = [CCMenuItemImage itemWithNormalImage:Shop_Menu selectedImage:Shop_Menu_Selected target:self selector:@selector(shopMenu)];
    [shopMenu setPosition:CGPointMake(winSize.width/8*7, winSize.height/7*5)];
    
    CCMenuItemImage *sinaMenu = [CCMenuItemImage itemWithNormalImage:Sina_Menu selectedImage:Sina_Menu_Selected target:self selector:@selector(sinaMenu)];
    [sinaMenu setPosition:CGPointMake(winSize.width/28*25, winSize.height/56*31)];
    
    CCMenuItemImage *gameCenterMenu = [CCMenuItemImage itemWithNormalImage:Center_Menu selectedImage:Center_Menu_Selected target:self selector:@selector(resetUserData)];
    [gameCenterMenu setPosition:CGPointMake(winSize.width/8*7, winSize.height/28*11)];
    
    CCMenuItemImage *playMenu = [CCMenuItemImage itemWithNormalImage:Play_Menu selectedImage:Play_Menu_Selected target:self selector:@selector(playMenu)];
    [playMenu setPosition:CGPointMake(winSize.width/5*4, winSize.height/7)];
    
    CCMenu *menu = [CCMenu menuWithItems:settingMenu, shopMenu, sinaMenu, gameCenterMenu, playMenu, nil];
//    CCMenu *menu = [CCMenu menuWithItems:settingMenu, shopMenu, sinaMenu, playMenu, nil];
    [menu setPosition:CGPointZero];
    menu.tag = Tag_Menu;
    [self addChild:menu];
    
    NSString *soundImage = nil;
    NSString *selectedImage = nil;
    if ([CCFruitsData getSoundSettings]) {
        soundImage = Game_SoundOn;
        selectedImage = Game_SoundOff;
    }else {
        soundImage = Game_SoundOff;
        selectedImage = Game_SoundOn;
    }
    CCMenuItemImage *soundItem = [CCMenuItemImage itemWithNormalImage:soundImage selectedImage:selectedImage target:self selector:@selector(setSound: )];
    soundItem.tag = Tag_Sound;
    soundItem.position = settingMenu.position;
    
    CCMenuItemImage *developerItem = [CCMenuItemImage itemWithNormalImage:Game_DeveloperNormal selectedImage:Game_DeveloperPressed target:self selector:@selector(showDeveloper:)];
    developerItem.tag = Tag_Developer;
    developerItem.position = settingMenu.position;
    CCMenu *setMenu = [CCMenu menuWithItems:soundItem, developerItem,nil];
    setMenu.position = CGPointZero;
    [setMenu runAction:[CCHide action]];
    [self addChild:setMenu z:menu.zOrder - 1 tag:Tag_SetMenu];
    
#if 0
    CCMenuItemImage *rankItem = [CCMenuItemImage itemWithNormalImage:Game_RankingNormal selectedImage:Game_RankingPressed target:self selector:@selector(goToRank: )];
    rankItem.tag = Tag_Rank;
    rankItem.position = gameCenterMenu.position;
    CCMenuItemImage *achieveItem = [CCMenuItemImage itemWithNormalImage:Game_AchieveNormal selectedImage:Game_AchievePressed target:self selector:@selector(goToAchieve:)];
    achieveItem.tag = Tag_Achieve;
    achieveItem.position = gameCenterMenu.position;
    CCMenu *gameCenter = [CCMenu menuWithItems:rankItem, achieveItem, nil];
    gameCenter.position = CGPointZero;
    [gameCenter runAction:[CCHide action]];
    [self addChild:gameCenter z:menu.zOrder - 1 tag:Tag_GameCenterMenu];
#endif
 
    CCSprite *container = [CCSprite spriteWithFile:Game_IconContainer];
    container.tag = Tag_IconContainer;
    container.position = ccp(winSize.width, soundItem.position.y);
    [container runAction:[CCHide action]];
    [self addChild:container];

}

- (void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyBoardNotification 
- (void)keyBoardWillShow :(NSNotification *)aNotificatioin {

    CGRect roteViewFrame = self.roteView.frame;
    roteViewFrame.origin = CGPointMake(roteViewFrame.origin.x - 50, roteViewFrame.origin.y);
        
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.roteView.frame = roteViewFrame;
        CCSprite *shareBg = (CCSprite *)[self getChildByTag:Tag_ShareSinaBg];
        CCMoveBy *move = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(0, 50)];
        [shareBg runAction:move];

    }completion:^(BOOL finished) {
        [(CCMenu *)[self getChildByTag:Tag_Menu] setIsTouchEnabled: NO];

    }];
}

- (void)keyBoardWillHide: (NSNotification *)aNotification {

    CGRect roteViewFrame = self.roteView.frame;
    roteViewFrame.origin = CGPointMake(roteViewFrame.origin.x + 50, roteViewFrame.origin.y);
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.roteView.frame = roteViewFrame;
        CCSprite *shareBg = (CCSprite *)[self getChildByTag:Tag_ShareSinaBg];
        CCMoveBy *move = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(0, -50)];
        [shareBg runAction:move];

     
    }completion:^(BOOL finished) {
        [(CCMenu *)[self getChildByTag:Tag_Menu] setIsTouchEnabled: YES];
    }];

}

#pragma mark - 设置

- (void)resetUserData {
    [CCFruitsData resetDefaults];
}

- (void)settingMenu //设置按钮调用
{
    if (didShowSina) {
        return;
    }
    [CCMusic gameMenuClickMusic];
    CCMenu *menu = (CCMenu *)[self getChildByTag:Tag_SetMenu];
    CCNode *soundItem = [menu getChildByTag:Tag_Sound];
    CCNode *developerItem = [menu getChildByTag:Tag_Developer];
    CCSprite *container = (CCSprite *)[self getChildByTag:Tag_IconContainer];
    
    CCMoveBy *soundMove = nil;
    CCMoveBy *developerMove = nil;
    CCMoveBy *iconMove = nil;
    if (didShowSubItem) {
        
        developerMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(developerItem.contentSize.width*2, 0)];
        soundMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(soundItem.contentSize.width, 0)];
        iconMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(container.contentSize.width - soundItem.contentSize.width, 0)];
        
        [soundItem runAction:soundMove];
        [developerItem runAction:developerMove];

//        [container runAction:[CCSequence actions:iconMove, [CCCallBlock actionWithBlock:^{
//            [container removeFromParentAndCleanup:YES];
//        }],nil]];

        [menu runAction:[CCSequence actions:[CCDelayTime actionWithDuration:0.5],[CCHide action], [CCCallBlock actionWithBlock:^{
            [self reorderChild:menu z:menu.tag - 1];
        }],nil]];        
    }else {
        [self reorderChild:menu z:menu.tag + 1];
        [menu runAction:[CCShow action]];
        
        developerMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(-developerItem.contentSize.width*2, 0)];
        soundMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(-soundItem.contentSize.width, 0)];
        iconMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(- (container.contentSize.width - soundItem.contentSize.width), 0)];
        
//        [container runAction:iconMove];
        [soundItem runAction:soundMove];
        [developerItem runAction:developerMove];
     
    }
//    [soundItem runAction:soundMove];
//    [developerItem runAction:developerMove];
//    
    didShowSubItem = !didShowSubItem;
}

- (void)shopMenu //商店按钮调用
{
    if (didShowSina) {
        return;
    }
    [CCMusic gameMenuClickMusic];
    [[CCDirector sharedDirector] replaceScene:[GameShopLayer sceneWithBackType:kBackMainMenuType]];
}

- (void)sinaMenu //sina按钮调用
{
    if (didShowSina) {
        return;
    }
    didShowSina = YES;
    [CCMusic gameMenuClickMusic];
    [self loadShareView];
}

- (void)gameCenterMenu //gameCenter按钮调用
{
    if (didShowSina) {
        return;
    }
    CCMenu *menu = (CCMenu *)[self getChildByTag:Tag_GameCenterMenu];
    CCNode *rankItem = [menu getChildByTag:Tag_Rank];
    CCNode *achieveItem = [menu getChildByTag:Tag_Achieve];
    
    CCMoveBy *randMove = nil;
    CCMoveBy *achieveMove = nil;
    if (didShowGameCenterSubItem) {
        achieveMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(achieveItem.contentSize.width*2, 0)];
        randMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(rankItem.contentSize.width, 0)];
        
        [menu runAction:[CCSequence actions:[CCDelayTime actionWithDuration:0.5],[CCHide action], [CCCallBlock actionWithBlock:^{
            [self reorderChild:menu z:menu.tag - 1];
        }],nil]];
    }else {
        [self reorderChild:menu z:menu.tag + 1];
        [menu runAction:[CCShow action]];
        
        achieveMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(-achieveItem.contentSize.width*2, 0)];
        randMove = [CCMoveBy actionWithDuration:0.5 position:CGPointMake(-rankItem.contentSize.width, 0)];
    }
    [rankItem runAction:randMove];
    [achieveItem runAction:achieveMove];
    
    didShowGameCenterSubItem = !didShowGameCenterSubItem;
    
}

- (void)playMenu //开始游戏按钮调用
{
    if (didShowSina) {
        return;
    }
    [CCMusic gameMenuClickMusic];
    [[CCDirector sharedDirector] replaceScene:[GameOrdersScene scene]];
}

//sub 
- (void)setSound :(id)sender {
    if (didShowSina) {
        return;
    }
    CCMenuItemImage *item = (CCMenuItemImage *)sender ;
    [CCMusic gameMenuClickMusic];
    if (![CCFruitsData getSoundSettings]) {
        [item setNormalImage:[CCSprite spriteWithFile:Game_SoundOn]];
        [CCFruitsData turnOnSound];
    }else {
        [item setNormalImage:[CCSprite spriteWithFile:Game_SoundOff]];
        [CCFruitsData turnOffSound];
    }
    playBackMusic = !playBackMusic;
}

- (void)showDeveloper :(id)sender {
    if (didShowSina) {
        return;
    }
    [CCMusic gameMenuClickMusic];
    [[CCDirector sharedDirector] replaceScene:[GameResultScene scene:kResultFinished]];
}

- (void)goToRank :(id)sender {
    
}

- (void)goToAchieve :(id)sender {
    
}

#pragma mark - sinaRelative

- (void)loadShareView {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCSprite *shareBg = [CCSprite spriteWithFile:@"commonBG.png"];
    shareBg.tag = Tag_ShareSinaBg;
    
    shareBg.position = CGPointMake(winSize.width/2, winSize.height/2);
    [self addChild:shareBg];
    
    self.roteView = [[[UIView alloc] init] autorelease];
    self.roteView.size = CGSizeMake(shareBg.contentSize.width - 60*2, shareBg.contentSize.height - 90*2);
    
    self.textView = [[[UITextView alloc] init] autorelease];
    self.textView.size = CGSizeMake(shareBg.contentSize.width - 60*2, shareBg.contentSize.height - 90*2);
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.textColor = [UIColor blackColor];
    self.textView.delegate = self;
    self.textView.font = [UIFont boldSystemFontOfSize:14.0f];
    self.textView.text = @"我刚刚玩了《弹叮铛水果盘》，我觉得你的智力可能过不了几关，不信你来试试";
    [self.roteView addSubview:self.textView];
    
    self.roteView.transform=CGAffineTransformMakeRotation(CC_DEGREES_TO_RADIANS(-90));
    self.roteView.origin = CGPointMake(90, 60);
    [[[[CCDirector sharedDirector] view] window] addSubview:self.roteView];
    
    CCMenuItemImage *shareSprite = [CCMenuItemImage itemWithNormalImage:@"sharebutton.png" selectedImage:nil target:self selector:@selector(shareToSina)];
    shareSprite.tag = Tag_ShareSprite;
    shareSprite.position = CGPointMake(winSize.width/2, shareSprite.contentSize.height/2 + 10);
    
    CCMenuItemImage *backSprite = [CCMenuItemImage itemWithNormalImage:@"fanhui.png" selectedImage:nil target:self selector:@selector(backToMenu)];
    backSprite.tag = Tag_ShareBack;
    backSprite.position = CGPointMake(winSize.width - backSprite.contentSize.width - 10, backSprite.contentSize.height/2 + 10);
    
    CCMenu *menu = [CCMenu menuWithItems:shareSprite, backSprite,nil];
    menu.tag = Tag_ShareMenu;
    menu.position = CGPointZero;
    [self addChild:menu];
}

- (void)backToMenu {
    [(CCMenu *)[self getChildByTag:Tag_Menu] setIsTouchEnabled: YES];
    didShowSina = NO;
    [self cleanShareSprites];
}

- (void)shareToSina {
    [(CCMenu *)[self getChildByTag:Tag_Menu] setIsTouchEnabled: YES];
    [self cleanShareSprites];
    SinaWeibo *sinaweibo = [GameSingleton sharedSina];

    if ([sinaweibo isAuthorizeExpired]) {
        [sinaweibo requestWithURL:@"statuses/update.json"
                                            params:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"我正在玩弹叮当", @"status", nil]
                                        httpMethod:@"POST"
                                          delegate:self];

    }else {
        sinaweibo.delegate = self;
        [sinaweibo logIn];
     }
 }


- (void)cleanShareSprites {
    [self.textView removeFromSuperview];
    self.textView = nil;
    [self.roteView removeFromSuperview];
    self.roteView = nil;
    
    CCMenu *menu = (CCMenu *)[self getChildByTag:Tag_ShareMenu];
    [menu removeFromParentAndCleanup:YES];
    
    CCSprite *bg = (CCSprite *)[self getChildByTag:Tag_ShareSinaBg];
    [bg removeFromParentAndCleanup:YES];
}


#pragma mark - SinaWeiboRequest Delegate

- (void)request:(SinaWeiboRequest *)request didFailWithError:(NSError *)error
{
    didShowSina = NO;
 if ([request.url hasSuffix:@"statuses/update.json"])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[NSString stringWithFormat:@"Post status \"%@\" failed!", self.textView.text]
                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        
        NSLog(@"Post status failed with error : %@", error);
    }
   
}

- (void)request:(SinaWeiboRequest *)request didFinishLoadingWithResult:(id)result
{
    didShowSina = NO;
    if ([request.url hasSuffix:@"statuses/update.json"])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"发表成功"
                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
    }
    NSLog(@"finish loading result %@",result);

}

#pragma mark - SinaWeibo Delegate

- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
    [GameSingleton storeAuthData];
    [[GameSingleton sharedSina] requestWithURL:@"statuses/update.json"
                       params:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"我正在玩弹叮当", @"status", nil]
                   httpMethod:@"POST"
                     delegate:self];

    
}

- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboDidLogOut");
  
}

- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
{
    didShowSina = NO;
    NSLog(@"sinaweiboLogInDidCancel");
}

- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
{
    didShowSina = NO;
    NSLog(@"sinaweibo logInDidFailWithError %@", error);
}

- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
{
    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
    [GameSingleton removeAuthData];
}
#pragma mark - textViewDelegate
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [self.textView resignFirstResponder];
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.textView resignFirstResponder];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [self.textView resignFirstResponder];
        return NO;
    }
    return YES;
}


@end
