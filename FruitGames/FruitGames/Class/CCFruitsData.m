//
//  CCFruitsData.m
//  FruitGames
//
//  Created by 周凯俊 on 12-12-7.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "CCFruitsData.h"
#import "GameSingleton.h"

#define DJ_First_Key @"DJ_First_Key"
#define DJ_Key @"DJ_Key"

#define kSoundValue @"soundSetting"
#define kLoginDate @"loginDate"
@implementation CCFruitsData

+ (BOOL)getFirstDJ
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:DJ_First_Key];
}

+ (void)saveFirstDJ
{
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:DJ_First_Key];
    [CCFruitsData saveDJNum:3];
}

+ (void)saveDJNum:(NSInteger)djNum
{
    [[NSUserDefaults standardUserDefaults] setInteger:djNum forKey:DJ_Key];
}

+ (NSInteger)getDJNum
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:DJ_Key];
}
//开启声音
+ (void)turnOnSound {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kSoundValue];
}
//关闭声音
+ (void)turnOffSound {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kSoundValue];
}

+ (BOOL)getSoundSettings {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kSoundValue];
}

+ (void)setLoginDate {
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:kLoginDate];
}

+ (BOOL)shouldShowLoginAwards {
    BOOL awards = YES;
    NSDate *loginDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginDate];
    if (!loginDate) {
        [self setLoginDate];
        return awards;
    }else {
        NSTimeInterval interVal = [loginDate timeIntervalSinceNow];
        if (interVal > 24*60*60) {
            [self setLoginDate];
            return awards;
        }else {
            awards = NO;
            return awards;
        }
    }
}

+ (void)resetDefaults {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSoundValue];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLoginDate];
    [GameSingleton removeAuthData];
}
@end
