//
//  GameCompareScene.m
//  FruitGames
//
//  Created by 周凯俊 on 12-11-29.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "GameCompareScene.h"
#import "CCFruits.h"
#import "FruitSprite.h"
#import "GameResultScene.h"
#import "CCMusic.h"
#import "AppDelegate.h"
#define Background @"Memory_Background.png"
#define Frame_Left @"Compare_Frame_Left.png"
#define Frame_Right @"Compare_Frame_Right.png"
#define Wrong @"wrong.png"
//#define Star_Light @"starLight.png"
//#define Star_Dark @"starDark.png"
#define Star_Background @"对比界面小修改底板.png"

#define Chance_Num 5
#define Left_Frame_Tag 1
#define Right_Frame_Tag 2
#define Star_Background_Tag 3

#define Scene_Change_Time 2.5
#define SpriteHead @"对比界面小修改人物.png"

@implementation GameCompareScene

+ (CCScene*)scene:(NSArray *)_array
{
    CCScene *scene = [CCScene node];
    GameCompareScene *layer = [[GameCompareScene alloc] initWithArray:_array];
    [scene addChild:layer];
    [layer release];
    return scene;
}

- (id)initWithArray:(NSArray *)_array
{
    if(self = [super init])
    {
        AppController *controller = (AppController *)[UIApplication sharedApplication].delegate;
        [controller startLoadAd];

        [self addUI];
        [self addFruits:_array];
    }
    
    return self;
}

- (void)addUI
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *background = [CCSprite spriteWithFile:Background];
    [background setPosition:CGPointMake(winSize.width/2, winSize.height/2)];
    [self addChild:background];
    
    CCSprite *rightFrame = [CCSprite spriteWithFile:Frame_Right];
    CGSize cttSize = [rightFrame contentSize];
    [rightFrame setPosition:CGPointMake(winSize.width - cttSize.width/2, winSize.height/2)];
    [rightFrame setTag:Right_Frame_Tag];
    [self addChild:rightFrame];
    
    CCSprite *leftFrame = [CCSprite spriteWithFile:Frame_Left];
    cttSize = [leftFrame contentSize];
    [leftFrame setPosition:CGPointMake(cttSize.width/2, winSize.height/2)];
    [leftFrame setTag:Left_Frame_Tag];
    [self addChild:leftFrame];
    
    CCSprite *starBackground = [CCSprite spriteWithFile:Star_Background];
    cttSize = [starBackground contentSize];
    [starBackground setPosition:CGPointMake(winSize.width - cttSize.width, winSize.height/2)];
    [starBackground setTag:Star_Background_Tag];
    [self addChild:starBackground];
    
    for(NSInteger num = 0; num < Chance_Num; num++)
    {
        CCSprite *sprite = [CCSprite spriteWithFile:SpriteHead];
        [sprite setPosition:CGPointMake(cttSize.width/2, cttSize.height/10*(8 - num*2 + 1))];
        [sprite setTag:num];
        [starBackground addChild:sprite];
    }
    
    [CCMusic gameCompareLayerMusic];
}

- (void)addFruits:(NSArray *)_array
{
    NSArray *array = [[CCFruits shared] getFruits];
    CCSprite *leftFrame = (CCSprite*)[self getChildByTag:Left_Frame_Tag];
    CCSprite *rightFrame = (CCSprite*)[self getChildByTag:Right_Frame_Tag];
    
    NSInteger count = [array count];
    for(NSInteger num = 0; num < count; num++)
    {
        FruitSprite *sprite = (FruitSprite*)[array objectAtIndex:num];
        FruitSprite *newSprite = [FruitSprite spriteWithTexture:[sprite texture]];
        [newSprite setFramePoint:[sprite framePoint]];
        [newSprite setPosition:[self getLeftPointFromFramePoint:[newSprite framePoint]]];
        [leftFrame addChild:newSprite];
    }
    
    BOOL isSuccess = true;
    if([array count] != [_array count])
    {
        isSuccess = false;
    }
    
    count = [_array count];
    if (0 == count) {
        for (int i = 0; i < [array count];i++) {
            [[CCFruits shared] chanceNumSub];
        }
    }else {
        for(NSInteger num = 0; num < count; num++)
        {
            FruitSprite *sprite = (FruitSprite*)[_array objectAtIndex:num];
            FruitSprite *newSprite = [FruitSprite spriteWithTexture:[sprite texture]];
            [newSprite setFramePoint:[sprite framePoint]];
            [newSprite setPosition:[self getRightPointFromFramePoint:[newSprite framePoint]]];
            [rightFrame addChild:newSprite];
            
            if(![[CCFruits shared] fruitsCompare:newSprite])
            {
                [[CCFruits shared] chanceNumSub];
                [self addWrong:newSprite];
                isSuccess = false;
            }
        }

    }
//    for(NSInteger num = 0; num < count; num++)
//    {
//        FruitSprite *sprite = (FruitSprite*)[_array objectAtIndex:num];
//        FruitSprite *newSprite = [FruitSprite spriteWithTexture:[sprite texture]];
//        [newSprite setFramePoint:[sprite framePoint]];
//        [newSprite setPosition:[self getRightPointFromFramePoint:[newSprite framePoint]]];
//        [rightFrame addChild:newSprite];
//        
//        if(![[CCFruits shared] fruitsCompare:newSprite])
//        {
//            [[CCFruits shared] chanceNumSub];
//            [self addWrong:newSprite];
//            isSuccess = false;
//        }
//    }

    CCNode *background = [self getChildByTag:Star_Background_Tag];
    for(NSInteger num = 0; num < Chance_Num; num++)
    {
        CCNode *node = [background getChildByTag:num];
        CCDelayTime *delayAct = [CCDelayTime actionWithDuration:(num + 1)];
        
        CCCallBlock *callAct = [CCCallBlock actionWithBlock:^(void){ [node removeFromParentAndCleanup:true]; }];
        [self runAction:[CCSequence actions:delayAct, callAct, nil]];
    }
    
    CCDelayTime *delayAct = [CCDelayTime actionWithDuration:Scene_Change_Time];
    
    CCCallBlock *callAct = [CCCallBlock actionWithBlock:^(void){ [self result:isSuccess]; }];
    [self runAction:[CCSequence actions:delayAct, callAct, nil]];
}

- (void)addWrong:(FruitSprite *)sprite
{
    CGSize cttSize = [sprite contentSize];
    CCSprite *wrong = [CCSprite spriteWithFile:Wrong];
    [wrong setPosition:CGPointMake(cttSize.width/2, cttSize.height/2)];
    [sprite addChild:wrong];
}

- (void)result:(BOOL)_result
{
    CCLOG(@"result %i",_result);
    if(_result)
    {
        [[CCDirector sharedDirector] replaceScene:[GameResultScene scene:kResultSuccess]];
    }
    else
    {
        [[CCDirector sharedDirector] replaceScene:[GameResultScene scene:kResultFailed]];
    }
}

- (CGPoint)getLeftPointFromFramePoint:(FramePoint)_framePoint
{
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:Left_Frame_Tag];
    CGSize cttSize = [frameSprite contentSize];
    
    CGPoint point;
    switch(_framePoint)
    {
        case 0:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*16);
            break;
        }
        case 1:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*16);
            break;
        }
        case 2:
        {
            point = CGPointMake(cttSize.width/20*13, cttSize.height/80*16);
            break;
        }
        case 3:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*28);
            break;
        }
        case 4:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*28);
            break;
        }
        case 5:
        {
            point = CGPointMake(cttSize.width/20*13, cttSize.height/80*28);
            break;
        }
        case 6:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*41);
            break;
        }
        case 7:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*41);
            break;
        }
        case 8:
        {
            point = CGPointMake(cttSize.width/20*13, cttSize.height/80*41);
            break;
        }
        case 9:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*53);
            break;
        }
        case 10:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*53);
            break;
        }
        case 11:
        {
            point = CGPointMake(cttSize.width/20*13, cttSize.height/80*53);
            break;
        }
        case 12:
        {
            point = CGPointMake(cttSize.width/20*5, cttSize.height/80*66);
            break;
        }
        case 13:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*66);
            break;
        }
        case 14:
        {
            point = CGPointMake(cttSize.width/20*13, cttSize.height/80*66);
            break;
        }
    }
    return point;
}

- (CGPoint)getRightPointFromFramePoint:(FramePoint)_framePoint
{
    CCSprite *frameSprite = (CCSprite*)[self getChildByTag:Right_Frame_Tag];
    CGSize cttSize = [frameSprite contentSize];
    
    CGPoint point;
    switch(_framePoint)
    {
        case 0:
        {
            point = CGPointMake(cttSize.width/40*11, cttSize.height/80*16);
            break;
        }
        case 1:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*16);
            break;
        }
        case 2:
        {
            point = CGPointMake(cttSize.width/40*25, cttSize.height/80*16);
            break;
        }
        case 3:
        {
            point = CGPointMake(cttSize.width/40*11, cttSize.height/80*28);
            break;
        }
        case 4:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*28);
            break;
        }
        case 5:
        {
            point = CGPointMake(cttSize.width/40*25, cttSize.height/80*28);
            break;
        }
        case 6:
        {
            point = CGPointMake(cttSize.width/40*11, cttSize.height/80*41);
            break;
        }
        case 7:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*41);
            break;
        }
        case 8:
        {
            point = CGPointMake(cttSize.width/40*25, cttSize.height/80*41);
            break;
        }
        case 9:
        {
            point = CGPointMake(cttSize.width/40*11, cttSize.height/80*54);
            break;
        }
        case 10:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*54);
            break;
        }
        case 11:
        {
            point = CGPointMake(cttSize.width/40*25, cttSize.height/80*54);
            break;
        }
        case 12:
        {
            point = CGPointMake(cttSize.width/40*11, cttSize.height/80*67);
            break;
        }
        case 13:
        {
            point = CGPointMake(cttSize.width/20*9, cttSize.height/80*67);
            break;
        }
        case 14:
        {
            point = CGPointMake(cttSize.width/40*25, cttSize.height/80*67);
            break;
        }
    }
    return point;
}

@end
