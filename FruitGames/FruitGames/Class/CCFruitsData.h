//
//  CCFruitsData.h
//  FruitGames
//
//  Created by 周凯俊 on 12-12-7.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CCFruitsData : CCNode {
    
}

+ (BOOL)getFirstDJ;
+ (void)saveFirstDJ;
+ (void)saveDJNum:(NSInteger)djNum; //保存道具的个数
+ (NSInteger)getDJNum; //获取道具的个数

+ (void)turnOnSound;
+ (void)turnOffSound;
+ (BOOL)getSoundSettings;
+ (BOOL)shouldShowLoginAwards ;//每天登陆奖励
+ (void)resetDefaults;//reset
@end
