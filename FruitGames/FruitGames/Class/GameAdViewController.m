//
//  GameAdViewController.m
//  FruitGames
//
//  Created by Static Ga on 12-12-25.
//
//

#import "GameAdViewController.h"
#import "cocos2d.h"

@interface GameAdViewController ()

@end

@implementation GameAdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)loadView {
    [super loadView];
    self.view.backgroundColor = [UIColor redColor];
    self.view.frame = CGRectMake(0.0, 0.0, 320, 100);
#if 0
    //创建广告位1
    ghadView = [[GHAdView alloc] initWithAdUnitId:@"f22c46de59b47e861bdb4a665305165b" size:CGSizeMake(320.0, 100.0)];
    //设置委托
    ghadView.delegate = self;
    //请求广告
    [ghadView loadAd];
    //设置frame并添加到View中
    ghadView.frame = CGRectMake(0.0,0.0,320.0, 100.0);
    [self.view addSubview:ghadView];
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if 0
#pragma mark -GHAdViewDelegate required method
- (UIViewController *)viewControllerForPresentingModalView
{
    return self;
}
- (void)willPresentModalViewForAd:(GHAdView *)view
{
    NSLog(@"12");
}
//加载广告失败时调用
- (void)adViewDidFailToLoadAd:(GHAdView *)view {
    NSLog(@"adview failed");
}

//加载广告成功时调用
- (void)adViewDidLoadAd:(GHAdView *)view {
    NSLog(@"adview success");
}
#endif
@end
