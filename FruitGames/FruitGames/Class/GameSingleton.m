//
//  GameSingleton.m
//  FruitGames
//
//  Created by Static Ga on 12-11-28.
//
//

#import "GameSingleton.h"
#import "Contants.h"
#import "SinaWeibo.h"
@implementation GameSingleton
+ (SinaWeibo *)sharedSina {
    static SinaWeibo *sinaWeibo = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
         sinaWeibo = [[SinaWeibo alloc] initWithAppKey:kSinaAppKey appSecret:kSinaAppSecret appRedirectURI:kSinaredirect_URI ssoCallbackScheme:kSinaSsoCallbackScheme andDelegate:nil];
    });
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *sinaweiboInfo = [defaults objectForKey:kUserDefaultsSinaWeiboAuthData];
    if ([sinaweiboInfo objectForKey:kUserDefaultsAccessTokenKey] && [sinaweiboInfo objectForKey:kUserDefaultsExpirationDateKey] && [sinaweiboInfo objectForKey:kUserDefaultsUserIDKey])
    {
        sinaWeibo.accessToken = [sinaweiboInfo objectForKey:kUserDefaultsAccessTokenKey];
        sinaWeibo.expirationDate = [sinaweiboInfo objectForKey:kUserDefaultsExpirationDateKey];
        sinaWeibo.userID = [sinaweiboInfo objectForKey:kUserDefaultsUserIDKey];
    }
    return sinaWeibo;
}

+ (void)removeAuthData
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserDefaultsSinaWeiboAuthData];
}

+ (void)storeAuthData
{
    SinaWeibo *sinaweibo = [GameSingleton sharedSina];
    
    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
                              sinaweibo.accessToken, kUserDefaultsAccessTokenKey,
                              sinaweibo.expirationDate, kUserDefaultsExpirationDateKey,
                              sinaweibo.userID, kUserDefaultsUserIDKey,
                              sinaweibo.refreshToken, kUserDefaultsRefreshDateKey, nil];
    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:kUserDefaultsSinaWeiboAuthData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
