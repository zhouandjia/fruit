//
//  FruitSprite.h
//  FruitGames
//
//  Created by 周凯俊 on 12-11-24.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum
{
    Point0 = 0,
    Point1 = 1,
    Point2 = 2,
    Point3 = 3,
    Point4 = 4,
    Point5 = 5,
    Point6 = 6,
    Point7 = 7,
    Point8 = 8,
    Point9 = 9,
    Point10 = 10,
    Point11 = 11,
    Point12 = 12,
    Point13 = 13,
    Point14 = 14
}FramePoint;

@interface FruitSprite : CCSprite {
    FramePoint framePoint;
}

@property (nonatomic) FramePoint framePoint;

@end
