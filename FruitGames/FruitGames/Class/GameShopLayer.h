//
//  GameShopLayer.h
//  FruitGames
//
//  Created by Static Ga on 12-12-5.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameResultScene.h"

#import <StoreKit/StoreKit.h>

typedef enum  {
    kBackMainMenuType = 0,
    kBackPlayMenuType ,
    kBackResultMenuType,
}kBackMenuType;

typedef enum {
    IAP6 = 10,
    IAP12 ,
    IAP18,
}kBuyTag;
@interface GameShopLayer : CCLayer <SKProductsRequestDelegate,SKPaymentTransactionObserver>{
    int buyType;
}
@property (nonatomic, assign) kResultType result;
+ (CCScene *)sceneWithBackType:(kBackMenuType)type;
- (id)initWithType: (kBackMenuType)type ;
@end
