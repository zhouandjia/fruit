//
//  GameLoginAwardsScene.m
//  FruitGames
//
//  Created by Static Ga on 13-1-5.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "GameLoginAwardsScene.h"
#import "CCFruitsData.h"

#define BG @"commonBG.png"
#define Add1 @"add1.png"
#define Add3 @"add3.png"
#define Add5 @"add5.png"
#define Guess @"guess.png"
#define LoginIcon @"loginawards.png"
#define Back @"fanhui.png"
@implementation GameLoginAwardsScene
+ (CCScene *)scene {
    CCScene *scene = [CCNode node];
    GameLoginAwardsScene *layer = [[[GameLoginAwardsScene alloc] init] autorelease];
    [scene addChild:layer];
    return scene;
}

- (id)init {
    if (self = [super init]) {
        itemsArray = [[NSMutableArray alloc] init];
        [self addUI];
    }
    return self;
}

- (void)dealloc {
    [itemsArray release];
    [super dealloc];
}
- (void)addUI {
    
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCSprite *bg = [CCSprite spriteWithFile:BG];
    bg.position = ccp(winSize.width/2, winSize.height/2);
    [self addChild:bg];
    
    CCSprite *loginSprite = [CCSprite spriteWithFile:LoginIcon];
    loginSprite.position = ccp(winSize.width/2, bg.contentSize.height - loginSprite.contentSize.height/2);
    [bg addChild:loginSprite];
    
//    CGFloat centerX = 0;
//    CGFloat centerY = 0;
    
    CCMenuItemImage *guessSprite = [CCMenuItemImage itemWithNormalImage:Guess selectedImage:nil target:self selector:@selector(toggleTheItem:)];
    guessSprite.tag = 1001;
    CGFloat centerX = guessSprite.contentSize.width;
    CGFloat centerY = guessSprite.contentSize.height;
    CCMenu *menu = [CCMenu menuWithItems: nil];

    for (int i = 0 ; i < 3; i++) {
        guessSprite.position = ccp(centerX, centerY);
        centerX += guessSprite.contentSize.width*1.3;
        centerY = guessSprite.contentSize.height;
        [menu addChild:guessSprite];
        [itemsArray addObject:guessSprite];
        guessSprite =  [CCMenuItemImage itemWithNormalImage:Guess selectedImage:nil target:self selector:@selector(toggleTheItem:)];
        guessSprite.tag = 1002 + i;
    }
    
    CCMenuItemImage *backSprite = [CCMenuItemImage itemWithNormalImage:@"fanhui.png" selectedImage:nil target:self selector:@selector(backToMenu)];
    backSprite.position = ccp(bg.contentSize.width - backSprite.contentSize.width, backSprite.contentSize.height);
    [menu addChild:backSprite];
    
    menu.position = CGPointZero;
    [bg addChild:menu];
}

- (void)toggleTheItem: (id)sender {
    
    for (CCMenuItemImage *item in itemsArray) {
        [item setIsEnabled:NO];
    }
    
    CCMenuItemImage *item = (CCMenuItemImage *)sender;

    int x = arc4random() % 3;
    NSInteger djNum = [CCFruitsData getDJNum];

    switch (x) {
        case 0:
        {
            [item setNormalImage:[CCSprite spriteWithFile:Add1]];
            [CCFruitsData saveDJNum:(djNum + 1)];
        }
            break;
        case 1:
        {
            [item setNormalImage:[CCSprite spriteWithFile:Add3]];
            [CCFruitsData saveDJNum:(djNum + 3)];

        }
            break;
        case 2:
        {
            [item setNormalImage:[CCSprite spriteWithFile:Add5]];
            [CCFruitsData saveDJNum:(djNum + 5)];

        }
            break;
        default:
            break;
    }
    [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:3],[CCCallBlock actionWithBlock:^{
        [self backToMenu];
    }], nil]];
}

- (void)backToMenu {
    [self removeFromParentAndCleanup:YES];
}
@end
