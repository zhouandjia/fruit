//
//  GameResultScene.h
//  FruitGames
//
//  Created by Static Ga on 12-12-4.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum {
    kResultSuccess = 0,//本句成功
    kResultFailed,//本局失败
    kResultFinished,//整个游戏通关
}kResultType;

//判定结果界面
@interface GameResultScene : CCLayer {
    
}
//传0或者1，0：成功 1：失败
+ (CCScene *)scene :(kResultType)theResult;
- (id)initWithResult:(kResultType)result;
@end
