//
//  GameOrdersScene.m
//  FruitGames
//
//  Created by Static Ga on 12-11-18.
//
//

#import "GameOrdersScene.h"
#import "GameMemoryScene.h"
#import "GameAdViewController.h"
#import "CCMusic.h"
#import "UIView+Helper.h"
#import "AderSDK.h"
#import "AppDelegate.h"

#define Background @"youxiyemianbeijing.png"
//sprites
#define Salesperson @"youxijiemiandandingdang.png"
#define Customer @"youxijiemianshengdane.png"
#define NextStep @"duihuayexiayibu.png"

#define kTag_Dialogue 1001
#define kTag_Customer 1002
#define kTag_NextStep 1003

#define Move_Time 1.5
#define Scale_Time  0.5
@implementation GameOrdersScene

+ (CCScene *)scene {
    CCScene *scene = [CCScene node];
    GameOrdersScene *layer = [GameOrdersScene node];
    [scene addChild:layer];
    return scene;
}


- (id)init {
    if (self = [super init]) {
        AppController *controller = (AppController *)[UIApplication sharedApplication].delegate;
        [controller clearAd];
        
        [self addBG];
        [self addSprite];
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        NSLog(@"winSize %@",NSStringFromCGSize(winSize));
        GameAdViewController *adVC = [[GameAdViewController alloc] init];
        adVC.view.transform=CGAffineTransformMakeRotation(CC_DEGREES_TO_RADIANS(-90));
        adVC.view.center = CGPointMake(50, winSize.width/2);
//        [[[[CCDirector sharedDirector] view] window] addSubview:adVC.view];
    }
    return self;
}

- (void)onEnter
{
    [super onEnter];
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (void)onExit//使用注册的方法添加点击一定要在scene消失的时候，把注册的点击事件移除，最好不用这种触发事件
{
    [super onExit];
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];
}

//出现按钮

- (void)nextMenu {
    
}

- (void)addBG
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *backgroundSprite = [CCSprite spriteWithFile:Background];
    [backgroundSprite setPosition:CGPointMake(winSize.width/2, winSize.height/2)];
    [self addChild:backgroundSprite];
}

- (void)addSprite {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *salesPersonSprite = [CCSprite spriteWithFile:Salesperson];
    salesPersonSprite.position = CGPointMake(winSize.width/3, winSize.height/3);
    [self addChild:salesPersonSprite];
    
    CCSprite *customerSprite = [CCSprite spriteWithFile:Customer];
    customerSprite.position = CGPointMake(winSize.width + customerSprite.contentSize.width / 2, winSize.height/3);
    customerSprite.tag = kTag_Customer;
    [self addChild:customerSprite];
    
    CCSprite *dialogueSprite = [CCSprite spriteWithFile:@"duihua1.png"];
    dialogueSprite.position = CGPointMake(winSize.width/2,winSize.height/2 + dialogueSprite.contentSize.height);
    dialogueSprite.scale = 0.0;
    dialogueSprite.tag = kTag_Dialogue;
    [self addChild:dialogueSprite];
    
    [self customerStartMove];
    
}

- (void)customerStartMove {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCSprite *sprite = (CCSprite*)[self getChildByTag:kTag_Customer];
    CCMoveTo *move = [CCMoveTo actionWithDuration:Move_Time position:CGPointMake(winSize.width - sprite.contentSize.width/2, winSize.height/3)];
    CCCallFunc *moveFinished = [CCCallFunc actionWithTarget:self selector:@selector(customerMoveInFinished)];
    [sprite runAction:[CCSequence actions:move, moveFinished, nil]];
}

//蛋蛋鳄进来后，出现对话1
- (void)customerMoveInFinished {
    [CCMusic gameLayerTransMusic];
    CCSprite *sprite = (CCSprite *)[self getChildByTag:kTag_Dialogue];
    CCScaleTo *scale = [CCScaleTo actionWithDuration:Scale_Time scale:1.0];
    CCCallFunc *callBack = [CCCallFunc actionWithTarget:self selector:@selector(finished)];
    [sprite runAction:[CCSequence actions:scale, callBack,nil]];
}

//对话2
- (void)dialogue2 {
    [CCMusic gameLayerTransMusic];
    CCSprite *sprite = (CCSprite *)[self getChildByTag:kTag_Dialogue];
    [sprite removeFromParentAndCleanup:YES];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *dialogueSprite = [CCSprite spriteWithFile:@"duihua2.png"];
    dialogueSprite.position = CGPointMake(winSize.width - dialogueSprite.contentSize.width/2,winSize.height - dialogueSprite.contentSize.height/2);
    dialogueSprite.tag = kTag_Dialogue;
    dialogueSprite.scale = 0.0;
    [self addChild:dialogueSprite];
    
    CCScaleTo *scale = [CCScaleTo actionWithDuration:Scale_Time scale:1.0];
    CCCallFunc *callBack = [CCCallFunc actionWithTarget:self selector:@selector(finished)];
    [dialogueSprite runAction:[CCSequence actions:scale,callBack, nil]];
}

//对话2完成后，最后的对话3
- (void)lastDialogue {
    [CCMusic gameLayerTransMusic];
    CCSprite *sprite = (CCSprite *)[self getChildByTag:kTag_Dialogue];
    [sprite removeFromParentAndCleanup:YES];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *dialogueSprite = [CCSprite spriteWithFile:@"duihua3.png"];
    dialogueSprite.position = CGPointMake(winSize.width/2, winSize.height/2 + dialogueSprite.contentSize.height);
    dialogueSprite.tag = kTag_Dialogue;
    dialogueSprite.scale = 0.0;
    [self addChild:dialogueSprite];
    
    CCScaleTo *scale = [CCScaleTo actionWithDuration:Scale_Time scale:1.0];
    CCCallFunc *callBack = [CCCallFunc actionWithTarget:self selector:@selector(finished)];
    [dialogueSprite runAction:[CCSequence actions:scale,callBack, nil]];
}
//出现下一步按钮
- (void)goToNext {
    [CCMusic gameMenuClickMusic];
    CCSprite *sprite = (CCSprite *)[self getChildByTag:kTag_Dialogue];
    [sprite removeFromParentAndCleanup:YES];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCMenuItemImage *nextMenu = [CCMenuItemImage itemWithNormalImage:NextStep selectedImage:nil target:self selector:@selector(nextStep)];
    nextMenu.position = CGPointMake(winSize.width - nextMenu.contentSize.width, nextMenu.contentSize.height);
    nextMenu.rotation = 0;
    CCRotateBy *rote = [CCRotateBy actionWithDuration:0.7 angle:360*2];
    CCRotateBy *rote2 = [CCRotateBy actionWithDuration:0.8 angle:360];
    nextMenu.tag = kTag_NextStep;
    
    CCMenu *menu = [CCMenu menuWithItems:nextMenu, nil];
    menu.position = CGPointZero;
    [self addChild:menu];
    
    CCCallFunc *finishedCall = [CCCallFunc actionWithTarget:self selector:@selector(finished)];
    [nextMenu runAction:[CCSequence actions:rote,rote2, finishedCall,nil]];
}

- (void)nextStep {
    [[CCDirector sharedDirector] replaceScene:[GameMemoryScene scene]];
}

- (void)finished {
    dialogueFinished = YES;
    totoalTouches ++;
}

#pragma mark - CCTouch

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    return YES;
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    if (dialogueFinished) {
        dialogueFinished = NO;
        switch (totoalTouches) {
            case 1:
            {
                [self dialogue2];
            }
                break;
            case 2:
            {
                [self lastDialogue];
            }
                break;
            case 3:
            {
                [self goToNext];
            }
                break;
            case 4:
            {
                CGPoint location = [touch locationInView:[touch view]];
                location = [[CCDirector sharedDirector] convertToGL:location];
                
                CCSprite *touchSprite = (CCSprite*)[self getChildByTag:kTag_NextStep];
                CGRect rect;
                rect.origin = CGPointZero;
                rect.size = [touchSprite contentSize];
                location = [touchSprite convertToNodeSpace:location];
                if(!CGRectContainsPoint(rect, location))
                {
                    [self nextStep];
                }
            }
                break;
            default:
                break;
        }
    }
}


@end
