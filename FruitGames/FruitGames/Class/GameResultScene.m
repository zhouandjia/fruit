//
//  GameResultScene.m
//  FruitGames
//
//  Created by Static Ga on 12-12-4.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "GameResultScene.h"
#import "GamePlayScene.h"
#import "GameMemoryScene.h"
#import "CCFruits.h"
#import "HelpNumSprite.h"
#import "GameMenuScene.h"
#import "GameShopLayer.h"
#import "CCFruitsData.h"
#import "CCMusic.h"
#import "AppDelegate.h"

#define R_BG @"tongyong.png"

#define R_ResumeNormal @"chongxintiaozhan.png"
#define R_ResumePressed @"chongxintiaozhanPressed.png"

#define R_FangDaJingNormal @"fangdajing.png"

#define R_FangDaJingJiaoBiao @"fangdajingjiaobiao.png"

#define R_StoreNormal @"gouwuchefen.png"
#define R_StorePressed @"gouwuchefenPressed.png"

#define R_GoOnNormal @"jixu.png"
#define R_GoOnPressed @"jixuPressed.png"

#define R_Success @"guoguanxingxing.png"
#define R_Failed @"guoguanxingxing1.png"

#define R_SuccessDingDang @"guoguandandingdang.png"
#define R_FaileDingDang @"sibairenwu.png"

#define R_GameFinishedBG @"tongyongjiemian03.png"
#define R_GoBack @"fanhui.png"
@interface GameResultScene ()
@property (nonatomic, assign) kResultType result;
@end

@implementation GameResultScene

+ (CCScene *)scene :(kResultType)theResult{
    CCScene *scene = [CCNode node];
    GameResultScene *layer = [[[GameResultScene alloc] initWithResult:theResult] autorelease];
    [scene addChild:layer];
    return scene;
}

- (id)initWithResult:(kResultType)result {
    if (self = [super init]) {
        AppController *controller = (AppController *)[UIApplication sharedApplication].delegate;
        [controller startLoadAd];

        self.result = result;
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        CCSprite *bgSprite = nil;
        if (kResultFinished == self.result) {
            [CCMusic gameSucessMusic];
            //表示通关
            bgSprite = [CCSprite spriteWithFile:R_GameFinishedBG];
            bgSprite.position = ccp(winSize.width/2, winSize.height/2);

            [self addChild:bgSprite];
            
            CCMenuItemImage *backItem = [CCMenuItemImage itemWithNormalImage:R_GoBack selectedImage:nil target:self selector:@selector(goToMainMenu:)];
            backItem.position = ccp(winSize.width - backItem.contentSize.width*2, backItem.contentSize.height);
            CCMenu *menu = [CCMenu menuWithItems:backItem, nil];
            menu.position = CGPointZero;
            [self addChild:menu];
        }else {
            bgSprite = [CCSprite spriteWithFile:R_BG];
            bgSprite.position = ccp(winSize.width/2, winSize.height/2);

            [self addChild:bgSprite];
            
            [self setUI];
            
            NSString *normalContinueImageName = nil;
            NSString *pressedContinueImageName = nil;
            switch (result) {
                case kResultSuccess:
                {
                    [CCMusic gameSucessMusic];
//                    normalContinueImageName = R_GoOnNormal;
//                    pressedContinueImageName = R_GoOnPressed;
                }
                    break;
                case kResultFailed:
                {
                    [CCMusic gameFailedMusic];
                    normalContinueImageName = R_ResumeNormal;
                    pressedContinueImageName = R_ResumePressed;
                }
                    break;
                default:
                    break;
            }
            if ([[CCFruits shared] getChanceNum] <= 0) {
                normalContinueImageName = R_ResumeNormal;
                pressedContinueImageName = R_ResumePressed;
            }else {
                normalContinueImageName = R_GoOnNormal;
                pressedContinueImageName = R_GoOnPressed;
            }
            CCMenuItemImage *chanceMenu = [CCMenuItemImage itemWithNormalImage:R_FangDaJingNormal selectedImage:nil];
            HelpNumSprite *helpNumSprite = [HelpNumSprite sprite];
            CGSize cttSize = [chanceMenu contentSize];
            [helpNumSprite setPosition:CGPointMake(cttSize.width, cttSize.height)];
            [chanceMenu addChild:helpNumSprite];
            
            chanceMenu.position = ccp(2*chanceMenu.contentSize.width, chanceMenu.contentSize.height);
            CCMenuItemImage *continueMenu = [CCMenuItemImage itemWithNormalImage:normalContinueImageName selectedImage:pressedContinueImageName target:self selector:@selector(goOnGame:)];
            continueMenu.position = ccp(winSize.width/2, chanceMenu.contentSize.height);
            CCMenuItemImage *goToStore = [CCMenuItemImage itemWithNormalImage:R_StoreNormal selectedImage:R_StorePressed target:self selector:@selector(goToStore:)];
            goToStore.position = ccp(winSize.width - 2*goToStore.contentSize.width, goToStore.contentSize.height);
            CCMenu *menu = [CCMenu menuWithItems:chanceMenu, continueMenu, goToStore,nil];
            [menu setPosition:CGPointZero];
            [self addChild:menu];
            
        }
    }
    return self;
}

- (void)setUI {
    CCSprite *sprite = nil;
    
    if (kResultSuccess == self.result) {
        sprite = [CCSprite spriteWithFile:R_SuccessDingDang];
    }else {
        sprite = [CCSprite spriteWithFile:R_FaileDingDang];
    }
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    sprite.position = ccp(winSize.width - sprite.contentSize.width*3/2, winSize.height/2);
    [self addChild:sprite];
    
    //暂时标记剩余提示机会（放大镜），这个可能需要从plist里面读取
    NSInteger totoalChances = [[CCFruits shared] getChanceNum];
    NSLog(@"total chances");
    CCSprite *starSprite = [CCSprite spriteWithFile:R_Success];
    CGFloat centerX = winSize.width - sprite.contentSize.width*2 - starSprite.contentSize.width/2;
    for (int i = 0; i < totoalChances; i++) {
        starSprite = [CCSprite spriteWithFile:R_Success];
        starSprite.position = ccp(centerX, winSize.height/2);
        [self addChild:starSprite];
        centerX -= starSprite.contentSize.width;
    }
    for (int i = 0; i < 3 - totoalChances;i++) {
        starSprite = [CCSprite spriteWithFile:R_Failed];
        starSprite.position = ccp(centerX, winSize.height/2);
        centerX -= starSprite.contentSize.width;
        [self addChild:starSprite];
    }
}

#pragma mark - private

- (void)goOnGame: (id)sender {
    [CCMusic gameMenuClickMusic];
    switch (self.result) {
        case kResultSuccess:
        {
            [[CCFruits shared] indexAdd];
            if([[CCFruits shared] getIndex] > 15)
            {
                //跳转通关界面
                [[CCDirector sharedDirector] replaceScene:[GameResultScene scene:kResultFinished]];
            }
            else
            {
                [[CCDirector sharedDirector] replaceScene:[GameMemoryScene scene]];
            }
        }
            break;
        case kResultFailed:
        {
            
//            [[CCFruits shared] chanceNumSub];
            if([[CCFruits shared] getChanceNum] <= 0)//机会数量少于0时
            {
                NSLog(@"直接进行下一个1");
                [[CCFruits shared] clearFruits];
                [[[CCFruits shared] getFruits] removeAllObjects];
//                [[CCDirector sharedDirector] replaceScene:[GameMemoryScene scene]];
                [[CCDirector sharedDirector] replaceScene:[GameMenuScene scene]];

            }
            else
            {
                NSLog(@"直接进行下一个2");
//                [[CCDirector sharedDirector] replaceScene:[GamePlayScene scene]];
                //直接进行下一关
                [[CCFruits shared] indexAdd];
                [[CCDirector sharedDirector] replaceScene:[GameMemoryScene scene]];

            }
        }
            break;
        default:
            break;
    }
}

- (void)goToStore: (id)sender {
    [CCMusic gameMenuClickMusic];
    CCScene *scene = [CCScene node];
    GameShopLayer *layer = [[[GameShopLayer alloc] initWithType:kBackResultMenuType] autorelease];
    layer.result = self.result;
    [scene addChild:layer];
    [[CCDirector sharedDirector] replaceScene:scene];
}

- (void)setFailedUI {
    
}

- (void)goToMainMenu :(id)sender {
    [CCMusic gameMenuClickMusic];
    [[CCDirector sharedDirector] replaceScene:[GameMenuScene scene]];
}
@end
