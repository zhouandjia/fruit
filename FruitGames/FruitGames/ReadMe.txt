弹叮当记录文档
12.6 GameOrdersScene界面实现是没多大问题，但是Menu不是那样使用的，可以参考GameMenuScene界面Menu的写法。写点击逻辑的时候没有考虑，玩家如果没等你字出现还在不停的点击屏幕是会出现的状况，这里还有点bug。点击注册的bug我帮你修改过了，其他的没有修改。
    道具已经可以使用了。后面时间我把道具的个数那些接口给出，和把关卡做出来。
12.7 [[CCFruits shared] getChanceNum]是获取本次游戏的机会个数，GameResultScene里面我改了一些，通关界面你写好了添加进去
12.8 [HelpNumSprite sprite]是初始化帮助道具的角标的精灵，你哪个地方用到了道具角标的地方直接加入就可以了。明天我把加密解密的加入到CCFruitsData里面，CCFruitsData类是游戏数据保存本地的类，里面现在保存了第一次进入游戏和道具数量的数据。如有什么本地数据需要保存，方法写在该类里面。

12.9: 贾：通关界面已经完成：用的GameResult类，添加了一种result枚举类型，kResultFinished表示通关
12.10:贾：有三个问题需要：
            a,在首页，点击打分享到新浪微博后，弹出界面，如何不让分享界面下面的menu接收touch时间，如设置按钮。
            b，有返回上一个控制器的函数吗？还有，我的通关界面是写在了GameResult中，那么在GameResult中，能 这样吗，    [[CCDirector sharedDirector] replaceScene:[GameResultScene scene:kResultFinished]];在自己的方法中replace自己scene。
            c，改了一个bug，在GamePlayScene中，我都标记了。
12.27：1，果合：guohead.com
        用户名： buzzstudio@yeah.net
        密码：23993360
2，广告的view我写在了GameAdViewController中
3，内置购买写在了GameShopLayer中，这块你着重看看，我不确定写的咋样。

12-30:
1，在GameCompareScene中，    //让那5个小精灵头挨个消失，消失之后加载下一个页面    [self result:isSuccess];
这里有点问题。
2,差个每天登陆奖励

 还差：1每天登陆奖励、
2，内置购买代码我写了，你看看流程啥的
3，广告。果合sdk我删掉了，你重新下载个看看。
4，游戏流程有所改变，每次玩只有三次机会，就那三星星，如果星星数量为0，则从第一关开始，这里我都改过了。

12.31 
  周：水果对比的时候代表机会的星星取消掉了？
      回答12.10 a，笨办法：可以设置一个bool值，点击了微博分享的时候bool值为YES，而menu只有该bool为NO时，里面的方法才生效。另一个办法，点击微博分享的按钮时，先把屏幕截图，以截的图片为背景图生成另一个场景，用pushScene的方法切换，分享完以后在pop回来。
                b，可以那样写，其实是重新初始化了一个场景出来替换，只是图片是一样的而已。
    12.30 小精灵消失我已经修改好了。
          我看了下内购流程没什么问题，就是把保存道具的数据加入进去就行了，你去CCFruitData.h里面看下，里面我写了道具个数的获取方法和保存方法，你只要购买成功后，保存下就OK了。
          广告的问题，我今天晚上下载下SDK，明天再看看有时间修改写没。
